package bohr.engine;


import bohr.engine.game.GameState;
import bohr.engine.net.Network;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;

/**
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class GameEngine extends Observable implements Observer {
    private GameState gameState;
    private EngineState engineState;
    private Network network;

    public GameEngine() {
        super();
        engineState = new EngineState();
        gameState = new GameState();
        network = new Network(this);
    }


    // GETTERS AND SETTERS
    public EngineState getEngineState() {
        return engineState;
    }

    public Network getNetwork() {
        return network;
    }

    public GameState getGameSetup() {
        return gameState;
    }

    public void setGameSetup(GameState gameSetup) {
        this.gameState = gameSetup;
    }

    @Override
    public void update(Observable obs, Object obj) {

    }
}
