package bohr.engine.game;

import bohr.engine.util.observable.Observable;

/**
 *
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameState extends Observable{
	private int lastId;
	private int speed;
	private int level;
	private int points;
	private int seconds;
	private boolean finished;

	public GameState() {
		lastId = 1;
		speed = 0;
		level = 0;
		points = 0;
		seconds = 0;
		finished = false;
	}

	public int getLastId() {
		return lastId;
	}

	public void incID() {
		this.lastId++;
	}

	public void incLevel() {
		this.level++;
	}

	public void incSeconds() {
		this.seconds++;
	}

	public void setLastId(int lastId) {
		this.lastId = lastId;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean fin) {
		this.finished = fin;
	}
}
