package bohr.engine.net.protocol;

/**
 * clase de solicitudes
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Request {
    private String type;
    private String action;

    /**
     * Constructor de solicitudes
     * @param type El tipo de accion
     * @param action mensaje con la accion que se desea
     */
    public Request(String type, String action) {
        this.type = type;
        this.action = action;
    }

    public Request() {
        this.type = "";
        this.action = "";
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
