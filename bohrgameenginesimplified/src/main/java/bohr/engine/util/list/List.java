package bohr.engine.util.list;

import java.lang.reflect.Array;

/**
 * Created by curso on 14/09/2015.
 */
public class List<T> {
    public Node<T> head;

    /**
     * @author Isaac Trejos
     * @author Randall Alfaro
     * @author Kevin Arse
     * @author Luis Fernando Murillo
     * Class Constructor
     */
    public List() {
        this.head = null;
    }

    public List(T[] array) {
        this.head = null;
        for (T obj : array) {
            add(obj);
        }
    }

    public Node<T> getHead() {
        return head;
    }

    public void setHead(Node<T> head) {
        this.head = head;
    }

    /**
     * Clase encargada de insertar los nodos con datos dentro de una lista
     *
     * @param data
     */
    public void add(T data) {
        if (head == null) {
            head = new Node(data);
        } else {
            Node<T> tmp = head;
            while (tmp.getNextNode() != null) {
                tmp = tmp.getNextNode();
            }
            tmp.setNextNode(new Node(data));
        }
    }

    /**
     * Se encarga de buscar un dato en la lista
     *
     * @param pData dato utilizado para la busqueda
     * @return regresa el dato si se encuentra y null si no se encuentra el dato.
     */
    public T search(T pData) {
        if (head != null) {
            if (head.getData() == pData) {
                return head.getData();
            } else {
                Node<T> tmp = head;
                while (tmp.getNextNode() != null) {
                    if (tmp.getNextNode().getData() == pData) {
                        return tmp.getNextNode().getData();
                    } else {
                        tmp = tmp.getNextNode();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Metodo que se encarga de remover el elemento de la lista
     *
     * @param pos la posiciòn del dato que se desea eliminar
     */
    public void remove(int pos) {
        Node<T> tmp = head;
        int i = 0;
        while (tmp != null && i < pos) {
            tmp = tmp.getNextNode();
            i++;
        }
        if (tmp != null && tmp.getNextNode() != null)
            tmp.setNextNode(tmp.getNextNode().getNextNode());
    }

    /**
     * Metodo que se encarga de remover el elemento de la lista
     *
     * @param data la posiciòn del dato que se desea eliminar
     */
    public void remove(T data) {
        if (head == null)
            return;
        Node<T> tmp = head;
        if (tmp.getData().equals(data)) {
            head = tmp.getNextNode();
        }
        while (tmp != null && tmp.getNextNode() != null &&
                !tmp.getNextNode().getData().equals(data)) {
            tmp = tmp.getNextNode();
        }
        if (tmp != null && tmp.getNextNode() != null)
            tmp.setNextNode(tmp.getNextNode().getNextNode());
    }

    /**
     * Metodo que se encarga de convertir la lista en un array
     */
    public T[] toArray() {
        int length = size();
        Object[] array = new Object[length];
        Node<T> tmp = head;
        for (int i = 0; i < length; i++) {
            array[i] = tmp.getData();
            tmp = tmp.getNextNode();
        }
        return (T[]) array;
    }

    /**
     * Metodo que se encarga de convertir la lista en un array
     */
    public int size() {
        int i = 0;
        Node<T> tmp = head;
        while (tmp != null) {
            i++;
            tmp = tmp.getNextNode();
        }
        return i;
    }
}
