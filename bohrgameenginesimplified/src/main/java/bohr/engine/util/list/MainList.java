package bohr.engine.util.list;

import com.google.gson.Gson;

import bohr.engine.admin.User;

/**
 * Created by randall on 29/09/2015.
 */
public class MainList {
    static private List<User> users = new List<>();

    public static void main(String[] args) {
        users.add(new User());
        users.add(new User());
        users.add(new User());
        users.add(new User());
        users.add(new User());
        System.out.println(new Gson().toJson(users.toArray())  );
    }
}
