package bohr.engine.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bohr.engine.admin.GeoPoint;

/**
 * Clase util esta clase esta encargada de las utilidades utlizadas en el juego
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Util {

    //VALIDACIOND DE TIPOS
    static String doublePattern = "([0-9]*)\\.([0-9]*)";
    static String integerPattern = "([0-9]*)";

    public static boolean isInteger(String str) {
        return Pattern.matches(integerPattern, str);
    }

    public static boolean isDouble(String str) {
        return Pattern.matches(doublePattern, str);
    }

    public static void println(String msg) {
        System.out.println(msg);
    }

    /**
     * Carga las propiedades utilizadas en la app
     *
     * @param propFileName es el nombre de la propiedad que utilzamos
     * @return las propiedades
     */
    public static Properties getProperties(String propFileName) {
        InputStream inputStream = null;
        Properties properties = new Properties();
        try {
            // String propFileName = "config.properties";
            //inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            inputStream = new FileInputStream(new File("").getAbsolutePath() + "/" + propFileName);
            if (inputStream == null) {
                System.out.println("inputStream == null");
                return null;
            }
            properties.load(inputStream);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return null;
        } finally {
            try {
                inputStream.close();
            } catch (Exception e) {
                System.out.println("Exception: " + e);
                return null;
            }
        }
        return properties;
    }

    //metodo para saber si se esta cerca del recurso

    /**
     * Calcula la distancia entre los puntos
     *
     * @param pointOne   la ubicacion en el gps
     * @param pointTwo   el punto donde se encuentra el recurso
     * @param pointTwo   el punto donde se encuentra el recurso
     * @param radioRange radio limite.
     * @return retorna la distancia maxima
     */
    public static boolean distanceInsideRange(GeoPoint pointOne, GeoPoint pointTwo, double radioRange) {
        /*System.out.println(distanceBetweenPoints(pointOne, pointTwo) + " - " + radioRange + " = " +
                        (distanceBetweenPoints(pointOne, pointTwo) - radioRange)
        );*/
        return distanceBetweenPoints(pointOne, pointTwo) <= radioRange;
    }

    public static double distanceBetweenPoints(GeoPoint pointOne, GeoPoint pointTwo) {
        double mPla = Math.toRadians(pointOne.getLatitude());
        double rPla = Math.toRadians(pointTwo.getLatitude());
        double rPlo = Math.toRadians(pointOne.getLongitude());
        double mPlo = Math.toRadians(pointTwo.getLongitude());
        return (2 * 6371000 * Math.asin(Math.sqrt(Math.pow(Math.sin((mPla - rPla) / 2), 2) +
                Math.cos(mPla) * Math.cos(rPla) * Math.pow(Math.sin((mPlo - rPlo) / 2), 2))));
    }


    //puntos random donde apareceran los recursos

    /**
     * Puntos random en donde aparecen los recursos
     *
     * @param min minimo punto en el mapa
     * @param max maximo punto en el mapa
     * @return los puntos donde se crearan los recursos
     */
    public static double randomPoint(double min, double max) {
        double intPart = new Random().nextInt((int) (max - min) + 1) + min;
        double floatPart = new Random().nextInt(99999) / 100000.0;
        double point = intPart + floatPart;
        return point;
    }


}
