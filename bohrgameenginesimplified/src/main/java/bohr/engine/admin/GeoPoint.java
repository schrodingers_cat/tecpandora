package bohr.engine.admin;

import java.io.Serializable;

/**
 * Clase utilizada para manejar los puntos en el mapa
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GeoPoint implements Serializable {
    private double latitude;
    private double longitude;

    /**
     * Constructor de la clase
     *
     * @param _longitud longitud del punto en el mapa
     * @param _latitud  latitud del punto en el mapa
     */
    public GeoPoint(double _latitud, double _longitud) {
        this.latitude = _latitud;
        this.longitude = _longitud;
    }

    /**
     * Constructor auxiliar de la clase
     */
    public GeoPoint() {
        this.latitude = 0;
        this.longitude = 0;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
