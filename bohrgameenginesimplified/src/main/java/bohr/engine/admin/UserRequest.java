package bohr.engine.admin;

/**
 * Clase utillizada para las solicitudes del usuario
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 *         Clase utillizada para
 */
public class UserRequest {
    private int id;
    private GeoPoint geoPoint;
    private String username;
    private int clanId;
    private String password;

    /**
     * constructor
     *
     * @param iD       ID del usuario
     * @param username Nombre del usuario
     * @param password Contraseña del usuarui
     */
    public UserRequest(int iD, String username, String password, int clanId) {
        id = iD;
        this.clanId = clanId;
        this.username = username;
        this.password = password;
        geoPoint = new GeoPoint();
    }

    /**
     * Solicitud de parte del usuario
     */
    public UserRequest() {
        clanId = -1;
        id = -1;
        this.username = "";
        this.password = "";
        geoPoint = new GeoPoint();
    }

    public UserRequest(int clanId) {
        super();
        this.clanId = clanId;
    }

    public UserRequest(User user) {
        clanId = -1;
        id = user.getId();
        this.username = user.getUsername();
        this.password = user.getPassword();
        geoPoint = user.getGeoPoint();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
