package game.model.threads;

import java.util.Random;

import bohr.engine.game.GameState;
import bohr.engine.util.Util;
import game.model.main.ServerGame;
import bohr.engine.admin.GeoPoint;

/**
 * Clase que controla los hilos del juego para la creacion de recursos en el mapa
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ServerGameThread extends Thread {
    private ServerGame game;
    private int add;
    private GameState state;
    private int timer;
    private int amount;

    /**
     * Constructor
     *
     * @param game
     */
    public ServerGameThread(ServerGame game) {
        this.game = game;
        add = 1;
        timer = 0;
        amount = 0;
        state = game.getGameState();
    }

    /**
     * Inicia el hilo encargado de la creacion de recursos
     */
    public void run() {
        while (!state.isFinished()) {
            if (timer % add == 0 && amount < 10000) {
                createRandomResource();
            }
            //System.out.println(new Gson().toJson(game.getResources()));
           /* if (amount == 99)
                System.out.println("*****" + game.getResources().size());*/
            sleep();
        }
    }

    public void createRandomResource() {
        GeoPoint geoPoint;
        //Máximo y mínimo deben venir de un java properties
        if (new Random().nextInt(100) < 35)
            geoPoint = new GeoPoint(Util.randomPoint(9, 10), -1 * Util.randomPoint(83, 85));
        else
            geoPoint = new GeoPoint(Util.randomPoint(9.30, 9.30), -1 * Util.randomPoint(83.30, 83.30));
        synchronized (game.getResources()) {
            game.newResource(geoPoint);
        }
        amount++;
    }

    //metodo para contar los segundos y dormir el hilo

    /**
     * Metodo encargado de dormir y distruir los hilos
     */
    private void sleep() {
        try {
            super.sleep(1000);
            timer++;
            state.incSeconds();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
