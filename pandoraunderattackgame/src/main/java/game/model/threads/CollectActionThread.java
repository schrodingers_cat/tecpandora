package game.model.threads;


import com.google.gson.GsonBuilder;

import bohr.engine.admin.User;
import bohr.engine.util.Util;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;
import game.model.entities.others.GameResource;
import game.model.entities.others.ClanCount;
import game.model.util.ClanSearch;

/**
 * Clase que contiene los datos de un hilo encargada de la recolección de recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class CollectActionThread {
    private Thread collectThread;
    private int time;
    //private CollectionActionState
    private GameResource resource;
    private List<User> usersCollecting;
    private List<Clan> clans;
    private List<ClanCount> clanCounting;
    private List<GameResource> resources;
    private double radioRange;
    private int collectQuantity;

    /**
     * Constructor
     *
     * @param resource   Recursos
     * @param clans      Lista de clanes
     * @param radioRange Radio maximo para la recoleción de recursos
     */
    public CollectActionThread(final GameResource resource,
                               final List<Clan> clans, double radioRange,
                               final List<GameResource> resources) {
        this.radioRange = radioRange;
        this.resources = resources;
        usersCollecting = new List<>();
        clanCounting = new List<>();
        this.resource = resource;
        this.clans = clans;
        time = 5000;
        collectQuantity = 15;
        collectThread = new Thread() {
            @Override
            public void run() {
                System.out.println("START_COLLECT" +
                                usersCollecting.size() + " - " +
                                resource.getQuantity() + " - " +
                                collectQuantity
                );
                while (usersCollecting.size() > 0 && resource.getQuantity() >= collectQuantity) {
                    System.out.println("COLLECT" +
                                    usersCollecting.size() + " - " +
                                    resource.getQuantity() + " - " +
                                    collectQuantity
                    );
                    cleanUsersByDistance();
                    most();
                    /*updateRange();
                    if (clanCounting.size() >= 2 && twoUpper()[0][0] > 2 * twoUpper()[1][0]) {//2*twoUpper()[1][0] o 2+twoUpper()[1][0]
                        distributeResourceByClan(twoUpper()[0][1]);
                        distributeResourceByUser(15 / twoUpper()[0][0]);
                    } else if (clanCounting.size() == 1) {
                        distributeResourceByClan(clanCounting.toArray()[0].getIdClan());
                        distributeResourceByUser(15 / clanCounting.toArray()[0].getQuantity());
                    }*/
                    Node<Clan> tmpClanNode = clans.getHead();
                    while (tmpClanNode != null) {
                        tmpClanNode.getData().updateResources(resource.getType(), 15);
                        resource.setQuantity(resource.getQuantity() - 15);
                        tmpClanNode = tmpClanNode.getNextNode();
                    }
                    //distributeResourceByUser(15);
                    sleep();
                }
                if (resource.getQuantity() < collectQuantity) {
                    synchronized (resources) {
                        resources.remove(resource);
                    }
                }
            }

            /**
             * Metodo para dormir el hilo
             */
            private void sleep() {
                try {
                    sleep(time);
                } catch (InterruptedException e) {
                }
            }
        };
    }

    /**
     * Metodo que actualiza la posición del miembro respecto al recurso
     */
    private void updateRange() {
        Node<Clan> tmpClanNode = clans.getHead();
        while (tmpClanNode != null) {
            Node<ClanMember> tmpClanMemberNode =
                    tmpClanNode.getData().getMembers().getHead();
            while (tmpClanMemberNode != null) {
                if (tmpClanMemberNode.getData().getResourcesCount() > 300) {
                    tmpClanMemberNode.getData().setRange(3);
                }
                if (tmpClanMemberNode.getData().getResourcesCount() > 700) {
                    tmpClanMemberNode.getData().setRange(2);
                }
                tmpClanMemberNode = tmpClanMemberNode.getNextNode();
            }
            tmpClanNode = tmpClanNode.getNextNode();
        }
    }

    /**
     * Metodo para la distribución de recursos a cada usuario
     *
     * @param quantity Cantidad de recurso que le entra al miembro
     */

    private void distributeResourceByUser(int quantity) {
        Node<User> tmpUserNode = usersCollecting.getHead();
        while (tmpUserNode != null) {
            Node<Clan> tmpClanNode = clans.getHead();
            while (tmpClanNode != null) {
                Node<ClanMember> tmpClanMemberNode =
                        tmpClanNode.getData().getMembers().getHead();
                while (tmpClanMemberNode != null) {
                    if (tmpUserNode.getData().getId() ==
                            tmpClanMemberNode.getData().getIdUser()) {
                        tmpClanMemberNode.getData().setResourcesCount(
                                tmpClanMemberNode.getData().getResourcesCount() + quantity);
                    }
                    tmpClanMemberNode = tmpClanMemberNode.getNextNode();
                }
                tmpClanNode = tmpClanNode.getNextNode();
            }
            tmpUserNode = tmpUserNode.getNextNode();
        }
    }

    /**
     * Metodo que distribuyen la cantidad de recusrsos a cada clan
     *
     * @param idClan ID del clan
     */
    private void distributeResourceByClan(int idClan) {
        Node<Clan> tmp = clans.getHead();
        while (tmp != null) {
            Clan clan = tmp.getData();
            if (clan.getId() == idClan) {
                resource.setQuantity(resource.getQuantity() - collectQuantity);
                clan.updateResources(resource.getType(), collectQuantity);
            }
            tmp = tmp.getNextNode();
        }
    }

    /**
     * Metodo que restringe el radio maximo para la recoleción de recursos para el usuario
     */
    private void cleanUsersByDistance() {
        Node<User> tmpUserNode = usersCollecting.getHead();
        while (tmpUserNode != null) {
            if (!tmpUserNode.getData().getSocket().isConnected() &&
                    !Util.distanceInsideRange(tmpUserNode.getData().getGeoPoint(), resource.getGeoPoint(), radioRange)) {
                usersCollecting.remove(tmpUserNode.getData());
            }
            tmpUserNode = tmpUserNode.getNextNode();
        }
    }

    /**
     * Metodo que crea una lista de los usuarios que recolectan por clan para encontrar lla mayoria de miembros
     */
    private void most() {
        Node<User> tmpUserNode = usersCollecting.getHead();
        while (tmpUserNode != null) {
            Clan clan = ClanSearch.getClanByMemberId(
                    tmpUserNode.getData().getId(), clans);
            Node<ClanCount> tmpClanCount =
                    clanCounting.getHead();
            while (tmpClanCount != null) {
                ClanCount clanCount = tmpClanCount.getData();
                if (clan.getId() == clanCount.getIdClan()) {
                    clanCount.setQuantity(clanCount.getQuantity() + 1);
                } else {
                    clanCounting.add(new ClanCount(
                            clan.getId(), 1));
                }
                tmpClanCount = tmpClanCount.getNextNode();
            }
            tmpUserNode = tmpUserNode.getNextNode();
        }

        //PRINT
        Node<ClanCount> tmpClanCount =
                clanCounting.getHead();
        while (tmpClanCount != null) {
            ClanCount mQ = tmpClanCount.getData();
            System.out.println("------" + new GsonBuilder().create().toJson(mQ));
            tmpClanCount = tmpClanCount.getNextNode();
        }
    }

    /**
     * Metodo que saca los dos clanes que se encuentran mayor en la lista para comparar.
     *
     * @return El clan mayor
     */
    private int[][] twoUpper() {
        int[][] upper = new int[2][2];
        upper[0] = new int[]{0, 0};
        upper[1] = new int[]{0, 0};
        Node<ClanCount> tmpClanMemberCollectingNode =
                clanCounting.getHead();
        while (tmpClanMemberCollectingNode != null) {
            ClanCount mQ = tmpClanMemberCollectingNode.getData();
            if (mQ.getQuantity() > upper[0][0]) {
                upper[1] = upper[0];
                upper[0] = new int[]{mQ.getQuantity(), mQ.getIdClan()};
            } else if (mQ.getQuantity() > upper[1][0]) {
                upper[1] = new int[]{mQ.getQuantity(), mQ.getIdClan()};
            }
            tmpClanMemberCollectingNode =
                    tmpClanMemberCollectingNode.getNextNode();
        }
        return upper;
    }

    public Thread getCollectThread() {
        return collectThread;
    }

    public GameResource getResource() {
        return resource;
    }

    public void setResource(GameResource resource) {
        this.resource = resource;
    }

    public List<User> getUsersCollecting() {
        return usersCollecting;
    }

    public void setUsersCollecting(List<User> usersCollecting) {
        this.usersCollecting = usersCollecting;
    }

    public List<Clan> getClans() {
        return clans;
    }

    public void setClans(List<Clan> clans) {
        this.clans = clans;
    }
}


