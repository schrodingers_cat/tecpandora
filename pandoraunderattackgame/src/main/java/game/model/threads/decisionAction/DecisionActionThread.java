package game.model.threads.decisionAction;


import bohr.engine.admin.User;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import bohr.engine.util.observable.Observable;
import game.model.entities.others.Vote;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;

/**
 * Clase que definne al clan, los rangos que puede obtener cada miembro del clan.
 * Las votaciones de los miebros respecto a su rango
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public abstract class DecisionActionThread extends Observable {
    private Thread decisionThread;
    private DecisionActionState state;
    private Clan clan;
    private List<Vote> votes;
    private int liderVote;
    private int coliderVote;
    private int puebloVote;
    private int esclavoVote;

    /**
     * Constructor
     *
     * @param clan       El clan en el cual se realiza la votacion
     * @param idThread   Numero del hilo
     * @param type       para que se esta realizando la votacion, para que acción.
     * @param liderVot   Voto del lider
     * @param coliderVot Voto del colider
     * @param puebloVot  Voto del pueblo
     * @param esclavoVot Voto del esclavo
     * @param seconds    Tiempo de la votación
     */
    public DecisionActionThread(final Clan clan, int idThread, final int type,
                                int liderVot, int coliderVot, int puebloVot,
                                int esclavoVot, int seconds) {
        this.clan = clan;
        state = new DecisionActionState(clan.getId(), idThread, type, seconds);
        votes = new List<>();
        this.liderVote = liderVot;
        this.coliderVote = coliderVot;
        this.puebloVote = puebloVot;
        this.esclavoVote = esclavoVot;
        decisionThread = new Thread() {
            @Override
            public void run() {
                startCollectAction();
            }

            /**
             * Inicio de las votaciones de un clan.
             * Dependiendo de la cantidad de votos, en base al rango del que tiene cada miebro del
             * clan, para al final tomar la decisión
             */
            private void startCollectAction() {
                while (state.getTime() < state.getSeconds()) {
                    try {
                        sleep(1000);
                        state.setTime(state.getTime() + 1);
                    } catch (InterruptedException e) {
//                        /System.out.println(e);
                    }
                }
                int pos = 0, neg = 0;
                Node<Vote> tmpVoteNode = votes.getHead();
                while (tmpVoteNode != null) {
                    if (tmpVoteNode.getData().getResponse()) {
                        Node<ClanMember> tmpClanMemberNode = clan.getMembers().getHead();
                        while (tmpClanMemberNode != null) {
                            if (tmpClanMemberNode.getData().getIdUser() == tmpVoteNode.getData().getUserId()) {
                                if (tmpClanMemberNode.getData().getRange() == 1)
                                    pos += liderVote;
                                if (tmpClanMemberNode.getData().getRange() == 2)
                                    pos += coliderVote;
                                if (tmpClanMemberNode.getData().getRange() == 3)
                                    pos += puebloVote;
                                if (tmpClanMemberNode.getData().getRange() == 4)
                                    pos += esclavoVote;

                            }
                            tmpClanMemberNode = tmpClanMemberNode.getNextNode();
                        }
                    } else {
                        Node<ClanMember> tmpClanMemberNode = clan.getMembers().getHead();
                        while (tmpClanMemberNode != null) {
                            if (tmpClanMemberNode.getData().getIdUser() == tmpVoteNode.getData().getUserId()) {
                                if (tmpClanMemberNode.getData().getRange() == 1)
                                    neg += liderVote;
                                if (tmpClanMemberNode.getData().getRange() == 2)
                                    neg += coliderVote;
                                if (tmpClanMemberNode.getData().getRange() == 3)
                                    neg += puebloVote;
                                if (tmpClanMemberNode.getData().getRange() == 4)
                                    neg += esclavoVote;

                            }
                            tmpClanMemberNode = tmpClanMemberNode.getNextNode();
                        }
                    }
                    tmpVoteNode = tmpVoteNode.getNextNode();
                }
                neg += Math.abs(votes.size() - clan.getMembers().size());
                //System.out.println("Votos ++= " + pos + "--" + "Votos --= " + neg);
                if (pos >= neg)
                    execAction(type);
            }
        };
        decisionThread.start();
    }

    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public int getLiderVote() {
        return liderVote;
    }

    public void setLiderVote(int liderVote) {
        this.liderVote = liderVote;
    }

    public int getColiderVote() {
        return coliderVote;
    }

    public void setColiderVote(int coliderVote) {
        this.coliderVote = coliderVote;
    }

    public int getPuebloVote() {
        return puebloVote;
    }

    public void setPuebloVote(int puebloVote) {
        this.puebloVote = puebloVote;
    }

    public int getEsclavoVote() {
        return esclavoVote;
    }

    public void setEsclavoVote(int esclavoVote) {
        this.esclavoVote = esclavoVote;
    }

    protected abstract void execAction(int type);

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public DecisionActionState getState() {
        return state;
    }

    public void setState(DecisionActionState state) {
        this.state = state;
    }

    public Thread getDecisionThread() {
        return decisionThread;
    }

    public void setDecisionThread(Thread decisionThread) {
        this.decisionThread = decisionThread;
    }
}
