package game.model.threads.attackAction;

import game.model.entities.clan.Clan;

/**
 * Created by ijtj03 on 29/09/15.
 */
public class AttackActionState {
    private int time;
    private int idClanA;
    private int idClanD;
    private int relicId;
    private int seconds;

    public AttackActionState() {
        this.time = 600;
        this.idClanA = -1;
        this.idClanD = -1;
        this.relicId = -1;
        this.seconds = 0;
    }
    public AttackActionState(int time, int idClanA, int relicId, int seconds) {
        this.time = time;
        this.idClanA = idClanA;
        this.relicId = relicId;
        this.seconds = seconds;
    }

    public int getIdClanD() {
        return idClanD;
    }

    public void setIdClanD(int idClanD) {
        this.idClanD = idClanD;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getIdClanA() {
        return idClanA;
    }

    public void setIdClanA(int idClanA) {
        this.idClanA = idClanA;
    }

    public int getRelicId() {
        return relicId;
    }

    public void setRelicId(int idClanB) {
        this.relicId = idClanB;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
