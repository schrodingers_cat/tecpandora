package game.model.threads.attackAction;


import bohr.engine.admin.User;
import bohr.engine.util.Util;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;
import game.model.entities.clan.Relic;

/**
 * Clase que contiene los datos de un hilo encargada de la recolección de recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class AttackActionThread {
    private AttackActionState state;
    private Thread attackThread;
    private List<Clan> clans;
    private List<User> users;
    private double randioRange;
    private List<Relic> relicList;


    /**
     * Constructor
     *
     * @param clans      Lista de clanes
     * @param radioRange Radio maximo para la recoleción de recursos
     */
    public AttackActionThread(int idClanA, int clanD, final int idRelic, final List<Clan> clans,
                              final List<Relic> relicList, double radioRange,
                              final List<User> users) {
        state.setIdClanA(idClanA);
        state.setRelicId(idRelic);
        this.randioRange = radioRange;
        this.clans = clans;
        this.users = users;
        this.relicList = relicList;
        Node<Relic> tmpRelicNodo = relicList.getHead();
        while (tmpRelicNodo != null) {
            if (tmpRelicNodo.getData().getIdRelic() == state.getRelicId()) {
                state.setIdClanD(tmpRelicNodo.getData().getClan().getId());
            }
            tmpRelicNodo = tmpRelicNodo.getNextNode();
        }
        attackThread = new Thread() {
            @Override
            public void run() {
                while (state.getSeconds() < state.getTime()) {
                    //Aqui deberia de avisar al clan que se va atacar que lo van ataca, durante 7 min
                    sleep();
                }
                if (membersInRelicRangebyClan(state.getIdClanA(), state.getRelicId()) >
                        membersInRelicRangebyClan(state.getIdClanD(), state.getRelicId())) {
                    //robo de la reliquia
                    //falta incluir el uso de las armas y bloqueos
                }
            }

            /**
             * Metodo para dormir el hilo
             */
            private void sleep() {
                try {
                    sleep(1000);
                    state.setSeconds(state.getSeconds() + 1);
                } catch (InterruptedException e) {
                }
            }
        };
    }

    private int membersInRelicRangebyClan(int idClan, int idRelic) {
        int membersInRange = 0;
        Node<Relic> tmpRelicNode = relicList.getHead();
        while (tmpRelicNode != null) {
            if (tmpRelicNode.getData().getIdRelic() == idRelic) {
                Node<User> tmpUserNode = users.getHead();
                while (tmpUserNode != null) {
                    Node<Clan> tmpClanNode = clans.getHead();
                    while (tmpClanNode != null) {
                        if (tmpClanNode.getData().getId() == idClan) {
                            Node<ClanMember> tmpClanMemberNode =
                                    tmpClanNode.getData().getMembers().getHead();
                            while (tmpClanMemberNode != null) {
                                if (tmpUserNode.getData().getId() ==
                                        tmpClanMemberNode.getData().getIdUser()) {
                                    if (Util.distanceInsideRange(tmpUserNode.getData().getGeoPoint(),
                                            tmpRelicNode.getData().getRelicPoint(), randioRange)) {
                                        membersInRange++;
                                    }
                                }
                                tmpClanMemberNode = tmpClanMemberNode.getNextNode();
                            }
                        }
                        tmpClanNode = tmpClanNode.getNextNode();
                    }
                    tmpUserNode = tmpUserNode.getNextNode();
                }
            }
            tmpRelicNode = tmpRelicNode.getNextNode();
        }
        return membersInRange;
    }

}
