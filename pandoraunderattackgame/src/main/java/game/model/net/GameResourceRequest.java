package game.model.net;

import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;

/**
 * Clase encargada de las solicitudes para la obtencion de recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameResourceRequest {
    private GameResource gameResource;
    private int user;
    private int clan;
    private GeoPoint geoPoint;

    /**
     * Constructor
     *
     * @param gameResource Recurso del juego
     * @param user         ID del usuario
     * @param geoPoint     la localizacion del recurso
     */
    public GameResourceRequest(GameResource gameResource, int user, int clan, GeoPoint geoPoint) {
        this.gameResource = gameResource;
        this.user = user;
        this.geoPoint = geoPoint;
        this.clan = clan;
    }

    public GameResourceRequest() {
        this(new GameResource(), -1, -1, new GeoPoint());
    }

    public int getUser() {
        return user;
    }

    public int getClan() {
        return clan;
    }

    public void setClan(int clan) {
        this.clan = clan;
    }

    public GameResource getGameResource() {
        return gameResource;
    }

    public void setGameResource(GameResource gameResource) {
        this.gameResource = gameResource;
    }

    public int getUserId() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }
}
