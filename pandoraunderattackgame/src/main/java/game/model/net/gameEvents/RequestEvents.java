package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.others.GameRequest;
import game.model.entities.others.Message;

/**
 * Created by randall on 04/10/2015.
 */
public class RequestEvents {

    /**
     * Obtiene todas las solicitudes hacia un usuarion
     *
     * @param user ID del usuario
     * @return solicitudes
     */
    public static DataResponse<GameRequest[]> getAllbyUserRequests(Network net, int user) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_USER,
                new GameRequest(-1, user, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest[]>>() {
                }.getType()
        );
    }

    /**
     * Obtiene todas las solicitudes enviadas a un clan
     *
     * @param clan ID del clan
     * @return la solicitud
     */
    public static DataResponse<GameRequest[]> getAllbyClanRequests(Network net, int userID, int clan) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_CLAN,
                new GameRequest(clan, userID, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest[]>>() {
                }.getType()
        );
    }

    /**
     * Enviar soliicitud de mensaje
     *
     * @param clan ID del clan
     * @param user ID del usuario
     * @return La solicitud
     */
    public static DataResponse<GameRequest> sendRequest(Network net, int clan, int user) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.SEND,
                new GameRequest(clan, user, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest>>() {
                }.getType()
        );
    }

    /**
     * Responde a la solicitud enresivida
     *
     * @param requestId ID de la solicitud
     * @param accepted  boolean True  o flase
     * @return retorna la solicitud
     */
    public static DataResponse<GameRequest> responseRequest(Network net, int requestId, boolean accepted) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.RESPONSE,
                new GameRequest(-1, -1, accepted, requestId));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest>>() {
                }.getType()
        );
    }

}
