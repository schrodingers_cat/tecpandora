package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.admin.GeoPoint;
import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.clan.Clan;

/**
 * Created by randall on 04/10/2015.
 */
public class ClanEvents {

    /**
     * Accion para crear un clan
     *
     * @param type     Tipo de reliquia
     * @param clanName Nombre del clan
     * @param owner    Lider del clan
     * @param geoPoint Localización del clan
     * @return el clan
     */
    public static DataResponse<Clan> createClan(Network net, int type, String clanName,
                                                int owner, GeoPoint geoPoint) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.CREATE,
                new Clan(0, type, clanName, owner, geoPoint));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Clan>>() {
                }.getType()
        );
    }

    /**
     * Accion para obtener toda la lista de clanes
     *
     * @return clanes
     */
    public static DataResponse<Clan[]> getAllClans(Network net, GeoPoint geoPoint) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.GET_ALL,
                new Clan());
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Clan[]>>() {
                }.getType()
        );
    }

    /**
     * Acción para obtener un clan en especifico
     *
     * @param name nombre del clan
     * @return clan
     */
    public static DataResponse<Clan> getClan(Network net, String name) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.GET,
                new Clan(-1, -1, name, -1, new GeoPoint()));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Clan>>() {
                }.getType()
        );
    }

    /**
     * Acción para loguearse a un clan en especifico
     *
     * @param userId   id del user
     * @param clanName nombre del clan
     * @return clan
     */
    public static DataResponse<Clan> loginClan(Network net, int userId, String clanName) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.CLAN_SERVICE, CommunicationProtocol.LOGIN,
                new Clan(-1, -1, clanName, userId, new GeoPoint()));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Clan>>() {
                }.getType()
        );
    }

}
