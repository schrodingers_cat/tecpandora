package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.admin.GeoPoint;
import bohr.engine.admin.User;
import bohr.engine.admin.UserRequest;
import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.net.GameResourceRequest;

/**
 * Created by randall on 04/10/2015.
 */
public class UserEvents {

    /**
     * Accion de entrar al clan
     *
     * @param username Nombre del usuario
     * @param pass     pase oo contraseña para acceder
     * @return Solicitud
     */
    public static DataResponse<User> signUp(
            Network net, String username, String pass) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.SIGN_UP,
                new UserRequest(-1, username, pass, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<User>>() {
                }.getType()
        );
    }

    /**
     * Obtiene toda la lista de usuarios
     *
     * @return solicitudes
     */
    public static DataResponse<User[]> getAllUsersByClan(
            Network net, int clanId) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.GET_ALL,
                new UserRequest(clanId));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<User[]>>() {
                }.getType()
        );
    }

    /**
     * Entrar a la cuenta del usuario
     *
     * @param username nombre del usuario
     * @param pass     contraseña del usuario
     * @return Solicitud
     */
    public static DataResponse<User> login(
            Network net, String username, String pass) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.LOGIN,
                new UserRequest(-1, username, pass, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<User>>() {
                }.getType()
        );
    }

    public static DataResponse<User> get(
            Network net, int userId) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.GET,
                new UserRequest(userId, "", "", -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<User>>() {
                }.getType()
        );
    }

    public static DataResponse<User> update(
            Network net, GeoPoint geoPoint) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.USER_SERVICE, CommunicationProtocol.UPDATE,
                new UserRequest(new User(geoPoint)));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<User>>() {
                }.getType()
        );
    }

}
