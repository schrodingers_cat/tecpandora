package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.others.GameRequest;
import game.model.entities.others.Message;

/**
 * Created by randall on 04/10/2015.
 */
public class MessageEvents {
    /**
     * Envia mensajes al servidor
     *
     * @param user    ID del usuario
     * @param clan    ID del clan
     * @param message Mensaje que se decea enviar
     * @return la peticion del mensaje
     */
    public static DataResponse<Message> sendMessage(
            Network net, int user, int clan, String message) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_MESSAGING_SERVICE, CommunicationProtocol.SEND,
                new Message(-1, user, clan, message));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Message>>() {
                }.getType()
        );
    }

    /**
     * Leer todos los mensajer del servidor
     *
     * @param clan ID de clan
     * @return el mensaje
     */
    public static DataResponse<Message[]> getAllMessaging(Network net, int clan) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_MESSAGING_SERVICE, CommunicationProtocol.GET_ALL,
                new Message(-1, -1, clan, ""));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<Message[]>>() {
                }.getType()
        );
    }
}
