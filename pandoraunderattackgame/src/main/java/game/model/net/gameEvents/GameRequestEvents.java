package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;

/**
 * Created by randall on 04/10/2015.
 */
public class GameRequestEvents {

    /**
     * Obtiene todas las solicitudes hacia un usuarion
     *
     * @param user ID del usuario
     * @return solicitudes
     */
    public static DataResponse<GameRequest[]> getAllbyUserRequests(Network net, int user) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_USER,
                new GameRequest(-1, user, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest[]>>() {
                }.getType()
        );
    }

    /**
     * Obtiene todas las solicitudes enviadas a un clan
     *
     * @param clan ID del clan
     * @return la solicitud
     */
    public static DataRequest<GameRequest[]> getAllbyClanRequests(Network net, int clan) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.GET_ALL_BY_CLAN,
                new GameRequest(clan, -1, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest[]>>() {
                }.getType()
        );
    }

    /**
     * Enviar soliicitud de mensaje
     *
     * @param clan ID del clan
     * @param user ID del usuario
     * @return La solicitud
     */
    public static DataRequest<GameRequest> sendRequest(Network net, int clan, int user) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.SEND,
                new GameRequest(clan, user, false, -1));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest>>() {
                }.getType()
        );
    }

    /**
     * Responde a la solicitud enresivida
     *
     * @param gameRequestId ID de la solicitud
     * @param accepted      boolean True  o flase
     * @return retorna la solicitud
     */
    public static DataRequest<GameRequest> responseRequest(
            Network net, int gameRequestId, boolean accepted) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_REQUESTS_SERVICE, CommunicationProtocol.RESPONSE,
                new GameRequest(-1, -1, accepted, gameRequestId));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameRequest>>() {
                }.getType()
        );
    }
}
