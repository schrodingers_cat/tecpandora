package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.clan.Clan;
import game.model.net.GameAction;
import game.model.threads.decisionAction.DecisionActionState;

/**
 * Created by randall on 04/10/2015.
 */
public class ActionEvents {
    /**
     * Acción de comprar un arma.
     *
     * @param user ID del usuario
     * @param clan ID del clan
     * @param type Tipo de arma que se desea comprar
     * @return Retorna la acción
     */
    public static DataResponse<DecisionActionState> buy(Network net, int user, int clan, int type) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.BUY,
                new GameAction(user, clan, -1, type, false));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<DecisionActionState>>() {
                }.getType());
    }

    /**
     * Resive todas las acciones
     *
     * @param clan ID del clan
     * @return La accion del juego
     */
    public static DataResponse<DecisionActionState[]> getAllByUserClanActions(
            Network net, int clan, int userId) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.GET_ALL,
                new GameAction(userId, clan, -1, -1, false));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<DecisionActionState[]>>() {
                }.getType());
    }

    /**
     * Accion de votación del juego
     *
     * @param idDecisionAction La ID del hilo de desiciones
     * @param idUser           ID del user
     * @param accepted         True o False dependiendo de la votación
     * @return la accion de la votación
     */
    public static DataResponse<DecisionActionState> voteGameAction(Network net, int idDecisionAction,
                                                                   int idUser, boolean accepted) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_ACTION_SERVICE, CommunicationProtocol.RESPONSE,
                new GameAction(idUser, -1, idDecisionAction, -1, accepted));
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<DecisionActionState>>() {
                }.getType());
    }
}
