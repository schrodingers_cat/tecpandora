package game.model.net.gameEvents;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.admin.GeoPoint;
import bohr.engine.admin.UserRequest;
import bohr.engine.net.Network;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.entities.others.GameResource;
import game.model.entities.others.Message;
import game.model.net.GameAction;
import game.model.net.GameResourceRequest;

/**
 * Clase encargado de los eventos generales del juego
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ResourcesEvents {

    /**
     * Acción para obtener toda los recursos
     *
     * @param geoPoint puntos de los recurso
     * @return el recurso
     */
    public static DataResponse<GameResource[]> getAllResources(
            Network net, GeoPoint geoPoint) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_RESOURCE_SERVICE, CommunicationProtocol.GET_ALL,
                new GameResourceRequest(
                        new GameResource(geoPoint), -1, -1, new GeoPoint()
                )
        );
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameResource[]>>() {
                }.getType()
        );
    }

    /**
     * Accion para colectar recurso
     *
     * @param idResource   ID del recurso
     * @param idUser       ID del usuario
     * @param userLocation Localización del recurso
     * @return recurso
     */
    public static DataResponse<GameResourceRequest> collect(
            Network net, int idResource, int idUser, GeoPoint userLocation) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_RESOURCE_SERVICE, CommunicationProtocol.COLLECT,
                new GameResourceRequest(
                        new GameResource(idResource), idUser, -1, userLocation
                )
        );
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameResourceRequest>>() {
                }.getType()
        );
    }

    public static DataResponse<GameResource[]> getAllByClanResources(Network net, int clanID) {
        DataRequest request = new DataRequest(
                CommunicationProtocol.GAME_RESOURCE_SERVICE, CommunicationProtocol.GET_ALL_BY_CLAN,
                new GameResourceRequest(
                        new GameResource(), -1, clanID, new GeoPoint()
                )
        );
        return new Gson().fromJson(
                net.sendEvent(new Gson().toJson(request)),
                new TypeToken<DataResponse<GameResource[]>>() {
                }.getType()
        );
    }

}
