package game.model.main;


import java.io.Serializable;

import bohr.engine.GameEngine;
import bohr.engine.util.list.List;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;

/**
 * Clase de cliente del juego encargada de los recursos y los clanes inicia el juego
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class ClientGame extends GameEngine implements Serializable {
    private static final long serialVersionUID = 1L;
    private Clan clan;
    private List<GameResource> resources;
    private List<Clan> clans;

    public ClientGame() {
        super();
        clan = new Clan();
        clans = new List<>();
        resources = new List<>();
    }

    public void setResources(List<GameResource> resources) {
        this.resources = resources;
    }

    public List<Clan> getClans() {
        return clans;
    }

    public void setClans(List<Clan> clans) {
        this.clans = clans;
    }

    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<GameResource> getResources() {
        return resources;
    }

    public void setMapResources(List<GameResource> resources) {
        this.resources = resources;
    }


    /*
    public void event(Object sender, String event, JSONObject obj) {
        switch (event) {
            case ServerProtocol.MESSAGES_COMMAND:
                notifyObservers(event, obj.toString());
                break;
            case ServerProtocol.MESSAGE_COMMAND:
                notifyObservers(event, obj.toString());
                break;
            default:
                break;
        }
    }*/

}
