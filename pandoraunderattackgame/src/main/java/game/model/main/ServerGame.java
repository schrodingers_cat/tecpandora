package game.model.main;

import bohr.engine.admin.User;

import java.util.Properties;
import java.util.Random;

import bohr.engine.GameEngine;
import bohr.engine.game.GameState;
import bohr.engine.util.Util;
import bohr.engine.util.list.List;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.threads.ServerGameThread;

/**
 * Clase inicianizadora del juego.
 * Inicia completamente el juego
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class ServerGame extends GameEngine {
    private ServerGameThread main_loop;
    private List<Clan> clans;
    private List<User> players;
    private List<GameResource> resources;
    private int quantiyResourceMin;
    private int quantiyResourceMax;
    private int id;

    /**
     * Constructor del juego
     *
     * @param clans     lista de clanes
     * @param players   lista de jugadores
     * @param resources lista de recursos
     */
    public ServerGame(List<Clan> clans, List<User> players, List<GameResource> resources) {
        this.clans = clans;
        this.players = players;
        this.resources = resources;
        quantiyResourceMin = 100;
        quantiyResourceMax = 400;
        main_loop = new ServerGameThread(this);
        main_loop.start();
        init();
        id = 1;
    }

    private void init() {
        for (int i = 0; i < 10000; i++) {
            main_loop.createRandomResource();
        }
    }

    public List<User> getPlayers() {
        return players;
    }

    //agrega los recursos

    /**
     * Crea un nuevo recursoo
     *
     * @param geoPoint puntos donde se ubicara el recurso
     */
    public void newResource(GeoPoint geoPoint) {
        GameResource resource = new GameResource(geoPoint, new Random().nextInt(3),
                id, quantiyResourceMin + new Random().nextInt(quantiyResourceMax));
        // , id, new Random(100).nextInt(500));
        //this.notifyEvent(GameProtocol.NEW_RESOURSE_COMMAND, resource.toJSON());
        resources.add(resource);
        id++;
    }

    public List<GameResource> getResources() {
        return resources;
    }

    public List<Clan> getClans() {
        return clans;
    }

    public ServerGameThread getMain_loop() {
        return main_loop;
    }

    /**
     * obtiene gamestate
     *
     * @return gamestate
     */
    public GameState getGameState() {
        return this.getGameSetup();
    }

    /**
     * Crea propiedades para el juefo
     * toma el maximo y minimo de los recursos
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        String strQuantityResourceMax = properties.getProperty("quantityResourceMax");
        String strQuantityResourceMin = properties.getProperty("quantityResourceMin");
        int qRMax = 500;
        int qRMin = 100;
        if (strQuantityResourceMax != null && Util.isInteger(strQuantityResourceMax)) {
            qRMax = Integer.valueOf(strQuantityResourceMax);
        }
        if (strQuantityResourceMin != null && Util.isInteger(strQuantityResourceMin)) {
            qRMin = Integer.valueOf(strQuantityResourceMin);
        }
        setMinMaxResourse(qRMin, qRMax);
    }

    /**
     * Maximo y minimo de la obtencion de recursos
     *
     * @param min minimo
     * @param max maximo
     */
    public void setMinMaxResourse(int min, int max) {
        this.quantiyResourceMin = min;
        this.quantiyResourceMax = max;
    }
}