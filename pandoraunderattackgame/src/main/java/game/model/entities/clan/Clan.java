package game.model.entities.clan;

import com.google.gson.Gson;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import bohr.engine.util.observable.Observable;
import game.model.entities.others.GameItem;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.entities.others.Message;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que almacena los datos de un clan
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Clan extends Observable {
    private int id;
    private int type;
    private String clanName;
    private int ownerId;
    private GeoPoint geoPoint;
    private transient List<GameResource> resources;
    private transient List<ClanMember> members;
    private transient List<GameItem> attackResourses;
    private transient List<Message> messages;

    /**
     * Constructor de la clase
     *
     * @param id       id del clan
     * @param type     tipo del clan
     * @param clanName nombre del clan
     * @param ownerId  creador del clan
     * @param geoPoint localizacion de la reliquia del clan
     */
    public Clan(int id, int type, String clanName, int ownerId, GeoPoint geoPoint) {
        this.id = id;
        this.type = type;
        this.clanName = clanName;
        this.ownerId = ownerId;
        this.geoPoint = geoPoint;
        resources = new List<>();
        members = new List<>();
        messages = new List<>();
        attackResourses = new List<>();

    }

    /**
     * Constructor auxiliar  de la clase
     */
    public Clan() {
        this.id = -1;
        this.type = 1;
        this.clanName = "";
        this.ownerId = -1;
        this.geoPoint = new GeoPoint();
        resources = new List<>();
        members = new List<>();
        messages = new List<>();
        attackResourses = new List<>();

    }

    public List<GameItem> getGameItems() {
        return attackResourses;
    }

    public void setAttackResourses(List<GameItem> attackResourses) {
        this.attackResourses = attackResourses;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<ClanMember> getMembers() {
        return members;
    }

    public void setMembers(List<ClanMember> members) {
        this.members = members;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }

    public void setResources(List<GameResource> resources) {
        this.resources = resources;
    }

    public List<GameResource> getResources() {
        return resources;
    }

    public void setType(int _type) {
        this.type = _type;
    }

    public void setClanname(String clanname) {
        this.clanName = clanname;
    }

    public void setOwnerId(int _owner) {
        this.ownerId = _owner;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public int getType() {
        return type;
    }

    public String getClanName() {
        return clanName;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    /**
     * Metodo para actualizar los recursos del clan
     *
     * @param type     tipo de recurso
     * @param quantity cantidad que se agrega de un recurso
     */
    public GameResource updateResources(int type, int quantity) {
        Node<GameResource> tmpResourcesNode = resources.getHead();
        while (tmpResourcesNode != null) {
            if (tmpResourcesNode.getData().getType() == type) {
                tmpResourcesNode.getData().setQuantity(
                        tmpResourcesNode.getData().getQuantity() + quantity);
                updateClan();
                return tmpResourcesNode.getData();
            }
            tmpResourcesNode = tmpResourcesNode.getNextNode();
        }
        GameResource gameResource = new GameResource(type, quantity);
        resources.add(gameResource);
        updateClan();
        return gameResource;
    }

    /**
     * Metodo para saber la cantidad de recursos segun el tipo
     *
     * @param type tipo de recurso
     * @return cantidad del recurso buscado
     */
    public int quantityResourceByType(int type) {
        Node<GameResource> tmpResourcesNode = resources.getHead();
        while (tmpResourcesNode != null) {
            if (tmpResourcesNode.getData().getType() == type) {
                return tmpResourcesNode.getData().getQuantity();
            }
            tmpResourcesNode = tmpResourcesNode.getNextNode();
        }
        return 0;
    }


    /**
     * Metodo para hacer un clan como JSON
     */
    private void updateClan() {
        DataResponse<Clan> update = new DataResponse<>(this);
        update.setResponse(CommunicationProtocol.CLAN_UPDATE);
        notifyObservers(new Gson().toJson(update));
    }
}
