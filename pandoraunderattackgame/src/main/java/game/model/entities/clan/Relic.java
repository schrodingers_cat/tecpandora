package game.model.entities.clan;

import bohr.engine.admin.GeoPoint;

/**
 * Clase que almacena los datos de una reliquia del clan
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class Relic {
    private GeoPoint relicPoint;
    private int idRelic;
    private Clan clan;

    /**
     * Constructor de la clase
     *
     * @param relicPoint punto donde se encuentra la reliquia en el mapa
     * @param idRelic identificacion de las reliquias
     * @param clan clan al quenpertenece la reliquia
     */
    public Relic(GeoPoint relicPoint, int idRelic, Clan clan) {
        this.relicPoint = relicPoint;
        this.idRelic = idRelic;
        this.clan = clan;
    }

    public void setIdRelic(int idRelic) {
        this.idRelic = idRelic;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }

    public Clan getClan() {
        return clan;
    }

    public void setRelicPoint(GeoPoint relicPoint) {
        this.relicPoint = relicPoint;
    }

    public int getIdRelic() {
        return idRelic;
    }

    public GeoPoint getRelicPoint() {
        return relicPoint;
    }
}
