package game.model.entities.others;

import java.io.Serializable;

/**
 * Clase que almacena los datos de un recurso de ataque
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameItem {
    private int type;
    private int resourseType;
    private int cost;

    /**
     * Constructor de la clase
     *
     * @param type         tipo del recurso de ataque
     * @param resourseType tipo del recurso para comprar el recurso de ataque
     * @param cost         costo para comprar el recurso de ataque
     */
    public GameItem(int type, int resourseType, int cost) {
        this.type = type;
        this.resourseType = resourseType;
        this.cost = cost;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getResourseType() {
        return resourseType;
    }

    public void setResourseType(int resourseType) {
        this.resourseType = resourseType;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}

