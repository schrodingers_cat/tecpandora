package game.model.entities.others;

/**
 * Clase que almacena los datos de una solicitud
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */

public class GameRequest {

    private int clanId;
    private int userId;
    private boolean active;
    private boolean response;
    private int idRequest;

    /**
     * Constructor auxiliar de la clase
     */
    public GameRequest() {
        this.clanId = -1;
        this.userId = -1;
        this.idRequest = -1;
        this.response = false;
        active = false;
    }

    /**
     * Constructor de la clase
     *
     * @param clan      id del clan
     * @param user      id del usuario
     * @param response  respuesta a la solicitud
     * @param idRequest id de la solicitud
     **/
    public GameRequest(int clan, int user, boolean response, int idRequest) {
        this.clanId = clan;
        this.userId = user;
        this.response = response;
        this.idRequest = idRequest;
        active = true;
    }

    public int getClanId() {
        return clanId;
    }

    public void setClanId(int clanId) {
        this.clanId = clanId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean positiveResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public int getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(int idRequest) {
        this.idRequest = idRequest;
    }
}