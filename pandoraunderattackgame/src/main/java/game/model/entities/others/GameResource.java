package game.model.entities.others;

import java.io.Serializable;

import bohr.engine.admin.GeoPoint;

/**
 * Clase que almacena los datos de un recurso
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameResource  {
    private GeoPoint geoPoint;
    private int type;
    private int id;
    private int quantity;

    /**
     * Constructor de la clase
     *
     * @param geoPoint punto donde esta el recurso
     * @param type     typo del recurso
     * @param id       id del recurso
     * @param quantity cantidad del recurso
     */
    public GameResource(GeoPoint geoPoint, int type, int id, int quantity) {
        this.geoPoint = geoPoint;
        this.type = type;
        this.id = id;
        this.quantity = quantity;
    }
/**
 * Constructor auxiliar de la clase
 */
    public GameResource(GeoPoint geoPoint) {
        this(geoPoint, 1, -1, 0);
    }

    public GameResource() {
        this(new GeoPoint(), 1, -1, 0);
    }

    public GameResource(int type, int quantity) {
        this(new GeoPoint(), type, -1, quantity);
    }

    public GameResource(int id) {
        this(new GeoPoint(), 1, id, 0);
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public int getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

}