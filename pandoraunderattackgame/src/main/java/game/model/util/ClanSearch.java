package game.model.util;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;

/**
 * Created by randall on 04/10/2015.
 */
public class ClanSearch {

    /**
     * Buscar un clan en la lista de Clanes por ID
     *
     * @param idClan ID del clan que se desea buscar
     * @param clans  Lista de clanes
     * @return null en caso de que no se encuentre y idClan en caso de que si
     */
    public static Clan getClanbyId(int idClan, List<Clan> clans) {
        Node<Clan> tmp = clans.getHead();
        while (tmp != null) {
            if (tmp.getData().getId() == idClan)
                return tmp.getData();
            tmp = tmp.getNextNode();
        }
        return null;
    }

    /**
     * Buscar un clan en la lista de clanes por el nombre
     *
     * @param clanName Nombre del clan
     * @param clans    lista de clanes
     * @return null en caso de que no se encuentre y clanName en caso de que si
     */
    public static Clan getClanByName(String clanName, List<Clan> clans) {
        Node<Clan> tmp = clans.getHead();
        while (tmp != null) {
            if (tmp.getData().getClanName().equals(clanName))
                return tmp.getData();
            tmp = tmp.getNextNode();
        }
        return null;
    }

    /**
     * Valida si un usuario es miembro de un clan
     *
     * @param userId Id del usuario
     * @param clan   Objeto clan
     * @return null en caso de que no se encuentre y clanName en caso de que si
     */
    public static ClanMember getClanByMemberId(int userId, Clan clan) {
        Node<ClanMember> tmp = clan.getMembers().getHead();
        while (tmp != null) {
            if (tmp.getData().getIdUser() == userId)
                return tmp.getData();
            tmp = tmp.getNextNode();
        }
        return null;
    }

    /**
     * Valida si un usuario es miembro de un clan
     *
     * @param userId Id del usuario
     * @param clans  Objeto clan
     * @return null en caso de que no se encuentre y clanName en caso de que si
     */
    public static Clan getClanByMemberId(int userId, List<Clan> clans) {
        Node<Clan> tmpClan = clans.getHead();
        while (tmpClan != null) {
            if (getClanByMemberId(userId, tmpClan.getData()) != null)
                return tmpClan.getData();
            tmpClan = tmpClan.getNextNode();
        }
        return null;
    }
}
