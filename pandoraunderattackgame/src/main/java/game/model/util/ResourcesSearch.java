package game.model.util;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.others.GameResource;
import game.model.threads.CollectActionThread;

/**
 * Created by randall on 04/10/2015.
 */
public class ResourcesSearch {
    /**
     * Buscar un recurso en la lista de recursos
     *
     * @param idClan    ID del clan que se desea buscar
     * @param resources lista de Recursos
     * @return null en caso de que no se encuentre y idClan en caso de que si
     */
    public static GameResource getGameResourceById(int idClan, List<GameResource> resources) {
        Node<GameResource> tmp = resources.getHead();
        while (tmp != null) {
            if (tmp.getData().getId() == idClan) {
                return tmp.getData();
            }
            tmp = tmp.getNextNode();
        }
        return null;
    }

    /**
     * Metodo encargado de obtener el hilo del collect
     *
     * @param idResource id del recurso.
     * @return respuesta del servidor; retorna hilo del colect o null
     */
    public static CollectActionThread getCollectActionThreadByResource(
            int idResource, List<CollectActionThread> collectActionThreads) {
        Node<CollectActionThread> tmpActionNode = collectActionThreads.getHead();
        while (tmpActionNode != null) {
            CollectActionThread collT = tmpActionNode.getData();
            if (collT.getResource().getId() == idResource) {
                return collT;
            }
            tmpActionNode = tmpActionNode.getNextNode();
        }
        return null;
    }


    /**
     * Metodo encargado de recolectar agrega players un collect
     *
     * @param playerId id del jugador.
     * @return respuesta del servidor; retorna nuevo hilo de collec.
     */
    public static CollectActionThread[] getCollectActionThreadByPlayer(int playerId) {
       /* ArrayList<CollectActionThread> collects = new ArrayList<>();
        for (CollectActionThread collT : collectActionThreads) {
            Node<User> tmpUserNode = collT.getUsersCollecting().getHead();
            while (tmpUserNode != null) {
                if (tmpUserNode.getData().getId() == playerId) {
                    collects.add(collT);
                }
                tmpUserNode = tmpUserNode.getNextNode();
            }
        }
        return collects;*/
        return null;
    }


}
