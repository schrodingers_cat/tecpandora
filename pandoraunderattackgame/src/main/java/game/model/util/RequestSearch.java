package game.model.util;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.others.GameRequest;

/**
 * Created by randall on 04/10/2015.
 */
public class RequestSearch {

    /**
     * Busca una solicitud
     *
     * @param requestId id de la solicitud
     * @return solicitud hecha por el usuario o null
     */
    public static GameRequest getRequestById(int requestId, List<GameRequest> requests) {
        Node<GameRequest> tmpRequestNode = requests.getHead();
        while (tmpRequestNode != null) {
            GameRequest gameRequest = tmpRequestNode.getData();
            if (gameRequest.getIdRequest() == requestId) {
                return gameRequest;
            }
            tmpRequestNode = tmpRequestNode.getNextNode();
        }
        return null;
    }

    /**
     * Metodo encargado de verificar que un usuario ha o no ha hecho una solicitud a un clan
     *
     * @param clan iD de clan al que se le hace la solicitud
     * @param user id del usuario que hace la solicitud
     * @return solicitud hecha por el usuario o null
     */
    public static GameRequest getRequestByUserClan(int user, int clan, List<GameRequest> requests) {
        Node<GameRequest> tmpRequestNode = requests.getHead();
        while (tmpRequestNode != null) {
            GameRequest gameRequest = tmpRequestNode.getData();
            if (gameRequest.getClanId() == clan && gameRequest.getUserId() == user) {
                return gameRequest;
            }
            tmpRequestNode = tmpRequestNode.getNextNode();
        }
        return null;
    }

    /**
     * Metodo que recorre la lista de las solicitudes para asi crear una lista de las solicitudes enviadas por un usuario
     *
     * @param user id del usuario que quiere saber las solicitudes enviadas
     * @return lista de las solicitudes enviadas por un usuario
     */
    public static List<GameRequest> getRequestByUser(int user, List<GameRequest> requests) {
        List<GameRequest> requestList = new List<>();
        Node<GameRequest> tmpRequestNode = requests.getHead();
        while (tmpRequestNode != null) {
            GameRequest gameRequest = tmpRequestNode.getData();
            if (gameRequest.getUserId() == user && gameRequest.isActive()) {
                requestList.add(gameRequest);
            }
            tmpRequestNode = tmpRequestNode.getNextNode();
        }
        return requestList;
    }

    /**
     * Metodo que recorre la lista de las solicitudes para asi crear una lista de las solicitudes de un clan
     *
     * @param clan id del clan que quiere saber las solicitudes
     * @return lista con las solicitudes recibidas por un clan
     */
    public static List<GameRequest> getActiveRequestByClan(int clan, List<GameRequest> requests) {
        List<GameRequest> req = new List();
        Node<GameRequest> tmpRequestNode = requests.getHead();
        while (tmpRequestNode != null) {
            if (tmpRequestNode.getData().getClanId() == clan &&
                    tmpRequestNode.getData().isActive()) {
                req.add(tmpRequestNode.getData());
            }
            tmpRequestNode = tmpRequestNode.getNextNode();
        }
        return req;
    }
}
