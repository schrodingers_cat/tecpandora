package game.model.util;

import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.others.Vote;
import game.model.threads.decisionAction.DecisionActionState;
import game.model.threads.decisionAction.DecisionActionThread;

/**
 * Created by randall on 04/10/2015.
 */
public class ActionSearch {
    public static List<DecisionActionState> getDesicionActionByClanId(
            int clanId, List<DecisionActionThread> desicions) {
        List<DecisionActionState> decisionActionStateList =
                new List<>();
        Node<DecisionActionThread> tmpNodeDecisionThread = desicions.getHead();
        while (tmpNodeDecisionThread != null) {
            if (tmpNodeDecisionThread.getData().getClan().getId() == clanId)
                decisionActionStateList.add(
                        tmpNodeDecisionThread.getData().getState()
                );
            tmpNodeDecisionThread = tmpNodeDecisionThread.getNextNode();
        }
        return decisionActionStateList;
    }

    public static DecisionActionThread getDecisionActionThreadById(
            int idAction,
            List<DecisionActionThread> desicions) {
        Node<DecisionActionThread> tmpVoteNode = desicions.getHead();
        while (tmpVoteNode != null) {
            if (tmpVoteNode.getData().getState().getActionId() == idAction) {
                return tmpVoteNode.getData();
            }
            tmpVoteNode = tmpVoteNode.getNextNode();
        }
        return null;
    }

    /**
     * Metodo encargado de obtener los votos de cada usuario
     *
     * @param userId id del usuario
     * @param votes  lista de votos por usuario
     * @return respuesta del servidor; retorna el voto del userId
     */
    public static Vote getVoteByUserID(int userId, List<Vote> votes) {
        Node<Vote> tmpVoteNode = votes.getHead();
        while (tmpVoteNode != null) {
            if (tmpVoteNode.getData().getUserId() == userId) {
                return tmpVoteNode.getData();
            }
            tmpVoteNode = tmpVoteNode.getNextNode();
        }
        return null;
    }

}
