package game.model.util;

import bohr.engine.admin.User;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;

/**
 * Created by randall on 04/10/2015.
 */
public class UserSearch {
    /**
     * Buscar un usuario en la lista de usuarios por ID
     *
     * @param idPlayer ID del jugadore que se desea buscar
     * @param users    La lista en la que se desea buscar
     * @return null en caso de que no se encuentre y idPlayer en caso de que si
     */
    public static User getUserById(int idPlayer, List<User> users) {
        Node<User> tmp = users.getHead();
        while (tmp != null)
            if (tmp.getData().getId() == idPlayer) {
                return tmp.getData();
            } else
                tmp = tmp.getNextNode();
        return null;
    }

    /**
     * Metodo encargado de buscar un usuario por el nombre de usuario y contraseña.
     *
     * @param username id de jugador.
     * @param password id de jugador.
     * @return respuesta del servidor; retorna null o el usuario a buscar.
     */
    public static User getPlayerByUserPassword(String username, String password, List<User> users) {
        Node<User> tmp = users.getHead();
        while (tmp != null) {
            if (tmp.getData().getUsername().equals(username)
                    && tmp.getData().getPassword().equals(password))
                return tmp.getData();
            else
                tmp = tmp.getNextNode();
        }
        return null;
    }

}
