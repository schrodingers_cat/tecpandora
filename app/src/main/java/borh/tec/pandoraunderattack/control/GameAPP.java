package borh.tec.pandoraunderattack.control;

import android.app.Application;

import game.model.main.ClientGame;

/**
 * Created by randall on 19/09/2015.
 */
public class GameAPP extends Application {

    private static GameAPP singleton;
    private ClientGame clientGame ;

    public GameAPP getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        clientGame =  new ClientGame();
        singleton = this;
    }

    public ClientGame getClientGame() {
        return clientGame;
    }
}
