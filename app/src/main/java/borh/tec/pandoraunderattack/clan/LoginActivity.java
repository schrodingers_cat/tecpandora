package borh.tec.pandoraunderattack.clan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import bohr.engine.admin.UserRequest;
import borh.tec.pandoraunderattack.R;
import borh.tec.pandoraunderattack.control.GameAPP;
import borh.tec.pandoraunderattack.util.MessageUI;
import game.model.entities.clan.Clan;
import game.model.main.ClientGame;
import game.model.net.gameEvents.ResourcesEvents;
import bohr.engine.admin.User;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;
import game.model.net.gameEvents.UserEvents;


public class LoginActivity extends AppCompatActivity {
    private ClientGame clientGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPolicy();
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        //clientGame.getEngineState().setServerHost("186.64.156.210");
        clientGame.getEngineState().setServerHost("186.64.156.210");
        //clientGame.getEngineState().setServerHost("192.168.0.103");
        setContentView(R.layout.activity_login);
        clickListeners();
    }

    private void initPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    private void clickListeners() {
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        final Context context = this;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clientGame.getNetwork().openConnection();
                if (!clientGame.getNetwork().isServerOn()) {
                    clientGame.getNetwork().openConnection();
                    if (!clientGame.getNetwork().isServerOn()) {
                        MessageUI.showMessage("There is a problem with the game server.", context);
                        clientGame.getNetwork().openConnection();
                    }
                }
                if (clientGame.getNetwork().isServerOn() && login())
                    startActivity(
                            new Intent(LoginActivity.this, MapsActivity.class));
                else
                    MessageUI.showMessage("Username and/or incorrect password", context);
            }
        });
        Button signIn = (Button) findViewById(R.id.btnSignIn);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!clientGame.getNetwork().isServerOn()) {
                    clientGame.getNetwork().openConnection();
                    if (!clientGame.getNetwork().isServerOn()) {
                        MessageUI.showMessage("There is a problem with the game server.", context);
                        clientGame.getNetwork().openConnection();
                    }
                }
                if (clientGame.getNetwork().isServerOn() && signIn()) {
                    MessageUI.showMessage("User created", context);
                } else {
                    MessageUI.showMessage("Username is been used by other user", context);
                }
            }
        });
    }

    /**
     * Metodo encargado de verificar que los datos para ingresar al sistema son correctos,
     * si son correctos agrega un objeto usuario al objeto ClientGame.EngineState
     *
     * @return True si los datos son correcto, False si no son correctos
     */
    private boolean login() {
        final TextView txtUsername = (TextView) findViewById(R.id.txtUsername);
        final TextView txtPassword = (TextView) findViewById(R.id.txtPassword);
        DataResponse<User> response =
                UserEvents.login(
                        clientGame.getNetwork(),
                        txtUsername.getText().toString(),
                        txtPassword.getText().toString());
        if (response.getCode() == CommunicationProtocol.NOT_FOUND_CODE)
            return false;
        clientGame.getEngineState().setUser(
                new User(response.getData().getId(), null,
                        response.getData().getUsername(),
                        response.getData().getPassword())
        );
        clientGame.setClan(new Clan());
        return true;
    }

    /**
     * Metodo encargado de verificar que los datos para registrarse al sistema sean válidos y únicos
     *
     * @return True si el registro fue exitoso, False si no fue exitoso
     */
    private boolean signIn() {
        final TextView txtUsername = (TextView) findViewById(R.id.txtUsername);
        final TextView txtPassword = (TextView) findViewById(R.id.txtPassword);
        DataResponse<User> response =
                UserEvents.signUp(
                        clientGame.getNetwork(),
                        txtUsername.getText().toString(),
                        txtPassword.getText().toString());
        if (response.getCode() == CommunicationProtocol.ALREADY_CREATED_CODE)
            return false;
        return true;
    }


}
