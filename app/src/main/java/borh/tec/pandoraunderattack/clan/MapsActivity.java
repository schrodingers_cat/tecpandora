package borh.tec.pandoraunderattack.clan;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import bohr.engine.admin.User;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.util.Util;
import borh.tec.pandoraunderattack.R;
import borh.tec.pandoraunderattack.control.GameAPP;
import borh.tec.pandoraunderattack.util.AnimationUtil;
import borh.tec.pandoraunderattack.util.MessageUI;
import borh.tec.pandoraunderattack.util.GameData;
import borh.tec.pandoraunderattack.util.GameView;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.entities.others.Message;
import game.model.main.ClientGame;
import game.model.net.gameEvents.ActionEvents;
import game.model.net.gameEvents.ClanEvents;
import game.model.net.gameEvents.RequestEvents;
import game.model.net.gameEvents.ResourcesEvents;
import game.model.net.gameEvents.MessageEvents;
import game.model.net.gameEvents.UserEvents;
import game.model.threads.decisionAction.DecisionActionState;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.util.ClanSearch;
import game.model.util.ResourcesSearch;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ClientGame clientGame;
    private PopupWindow popUpWindow;
    private int markerType;
    private FrameLayout fragment_menu;
    private Location lastLocation;
    boolean testExecuted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        markerType = 0;
        testExecuted = false;
        lastLocation = null;
        popUpWindow = new PopupWindow(this);
        fragment_menu = (FrameLayout) findViewById(R.id.fragment_menu);
        clientGame = ((GameAPP) getApplicationContext()).getClientGame();
        initActivity();
    }

    private void initActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (clientGame.getClan().getId() == -1)
                    setUpGameMenu();
                else setUpClanMenu();
                setUpMapIfNeeded();
                mapListeners();
            }
        });
    }

    ///*****************************************************
    //GAME MENU
    private void setUpGameMenu() {
        Fragment gameMenuFragment = new Fragment() {
            @Nullable
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                super.onCreateView(inflater, container, savedInstanceState);
                View view = inflater.inflate(R.layout.fragment_game_menu, container, false);
                container.removeAllViews();
                gameMenuListeners(view);
                return view;
            }
        };
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_menu, gameMenuFragment).commit();
    }

    private void gameMenuListeners(final View view) {
        Button btnClan = (Button) view.findViewById(R.id.btnClanMenu);
        final Activity _this = this;
        btnClan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clientGame.getClan().getId() != -1)
                    setUpClanMenu();
                else {
                    final View content = getLayoutInflater().inflate(R.layout.layout_clan_register, null);
                    if (mMap.getMyLocation() == null)
                        MessageUI.showToastMessage(_this,
                                "There is a problem getting your location, and is needed in order to register a clan");
                    else if (content != null && content.findViewById(R.id.txtLocation) != null) {
                        ((TextView) content.findViewById(R.id.txtLocation)).setText(
                                " Latitude: " + mMap.getMyLocation().getLatitude() + "\n" +
                                        "Longitude:" + mMap.getMyLocation().getLongitude()
                        );
                        ((Button) content.findViewById(R.id.btnCreateClan)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!clientGame.getNetwork().isServerOn())
                                            MessageUI.showToastMessage(_this,
                                                    "There is a problem with the game server.");
                                        else if (mMap.getMyLocation() == null)
                                            MessageUI.showToastMessage(_this,
                                                    "There is a problem getting your location");
                                        else if (registerClan(content, mMap.getMyLocation())) {
                                            MessageUI.showToastMessage(_this,
                                                    "Clan created successfully");
                                            popUpWindow.dismiss();
                                        } else {
                                            MessageUI.showToastMessage(_this,
                                                    "Clan name is used by other clan");
                                        }
                                    }
                                });
                            }
                        });
                        ((Button) content.findViewById(R.id.btnLoginClan)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!clientGame.getNetwork().isServerOn())
                                            MessageUI.showToastMessage(_this,
                                                    "There is a problem with the game server");
                                        else if (loginClan(content)) {
                                            popUpWindow.dismiss();
                                            MessageUI.showToastMessage(_this, "Logged with clan \"" +
                                                    clientGame.getClan().getClanName() + "\"");
                                        } else {
                                            MessageUI.showToastMessage(_this,
                                                    "Clan doesn't exist or you are not a member");
                                        }
                                    }
                                });
                            }
                        });
                        popUpWindow = GameView.showAndGetPopUpWindow(
                                getLayoutInflater(), popUpWindow, fragment_menu, content, true
                        );
                    }
                }
            }
        });
    }

    /**
     * Metodo encargado de verificar que los datos para ingresar un clan al sistema son correctos,
     * si son correctos agrega un objeto clan al objeto ClientGame
     *
     * @return True si los datos son correcto, False si no son correctos
     */
    private boolean registerClan(View view, Location location) {
        final TextView txtUsername = (TextView) view.findViewById(R.id.txtClanName);
        String clanName = txtUsername.getText().toString();
        int owner = clientGame.getEngineState().getUser().getId();
        DataResponse<Clan> response = ClanEvents.createClan(
                clientGame.getNetwork(), 0, clanName, owner,
                new GeoPoint(location.getLatitude(),
                        location.getLongitude()));
        if (response.getCode() == CommunicationProtocol.ALREADY_CREATED_CODE)
            return false;
        clientGame.setClan(response.getData());
        return true;
    }

    /**
     * Metodo encargado de verificar que los datos para ingresar un clan al sistema son correctos,
     * si son correctos agrega un objeto clan al objeto ClientGame
     *
     * @return True si los datos son correcto, False si no son correctos
     */
    private boolean loginClan(View view) {
        final TextView txtUsername = (TextView) view.findViewById(R.id.txtClanName);
        String clanName = txtUsername.getText().toString();
        DataResponse<Clan> response = ClanEvents.loginClan(
                clientGame.getNetwork(),
                clientGame.getEngineState().getUser().getId(),
                clanName
        );
        if (response.getCode() == CommunicationProtocol.NOT_FOUND_CODE ||
                response.getCode() == CommunicationProtocol.NOT_A_MEMBER_CODE)
            return false;
        clientGame.setClan(response.getData());
        return true;
    }

    ///*****************************************************
    //CLAN MENU
    private void setUpClanMenu() {
        Fragment clanMenuFragment = new Fragment() {
            @Nullable
            @Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                super.onCreateView(inflater, container, savedInstanceState);
                container.removeAllViews();
                View view = inflater.inflate(R.layout.fragment_clan_menu, container, false);
                clanMenuListeners(view);
                return view;
            }

        };
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_menu, clanMenuFragment).commit();
    }

    private void clanMenuListeners(final View view) {
        final Activity _this = this;
        //RESOURCES
        Button btnClanGameResources = (Button) view.findViewById(R.id.btnClanGameResources);
        btnClanGameResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View content = getLayoutInflater().inflate(R.layout.layout_list_items, null);
                final LinearLayout linearLayout = (LinearLayout) content.findViewById(R.id.scroll_container);
                final DataResponse<GameResource[]> response =
                        ResourcesEvents.getAllByClanResources(
                                clientGame.getNetwork(),
                                clientGame.getClan().getId()
                        );
                if (response.getData().length == 0) {
                    MessageUI.showToastMessage(_this, "You have no resources");
                } else {
                    popUpWindow = GameView.showAndGetPopUpWindow(
                            getLayoutInflater(), popUpWindow, fragment_menu, content, true
                    );
                    new Thread() {
                        @Override
                        public void run() {
                            while (content.isShown()) {
                                final DataResponse<GameResource[]> response =
                                        ResourcesEvents.getAllByClanResources(
                                                clientGame.getNetwork(),
                                                clientGame.getClan().getId()
                                        );
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        linearLayout.removeAllViews();
                                        for (GameResource gameResource : response.getData()) {
                                            linearLayout.addView(
                                                    getResourceView(gameResource, (ViewGroup) content));
                                        }
                                    }
                                });
                                sleep();
                            }
                        }

                        private void sleep() {
                            try {
                                sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
            }
        });
        //CHAT
        Button btnClanChat = (Button) view.findViewById(R.id.btnClanChat);
        btnClanChat.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final View content = getLayoutInflater().inflate(R.layout.layout_clan_chat, null);
                        //Que el scroll de los mensajes, empiece en la parte de abajo (en el último mensaje)
                        final ScrollView scrollView = (ScrollView) content.findViewById(R.id.scroll_container);
                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });

                        //Listener para enviar mensaje
                        final TextView txtSend = (TextView) content.findViewById(R.id.txtSend);
                        final Button btnSendMsg = (Button) content.findViewById(R.id.btnSendMsg);
                        btnSendMsg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MessageEvents.sendMessage(
                                        clientGame.getNetwork(),
                                        clientGame.getEngineState().getUser().getId(),
                                        clientGame.getClan().getId(),
                                        txtSend.getText().toString()
                                );
                                txtSend.setText("");
                            }
                        });

                        popUpWindow = GameView.showAndGetPopUpWindow(
                                getLayoutInflater(), popUpWindow, fragment_menu, content, false
                        );
                        new Thread() {
                            @Override
                            public void run() {
                                while (content.isShown()) {
                                    final View chatContent = getLayoutInflater().inflate(
                                            R.layout.layout_clan_chat_messaging, null);
                                    final TextView txtChat =
                                            (TextView) chatContent.findViewById(R.id.txtChat);
                                    //Muestra Mensajes traídos del server
                                    final DataResponse<Message[]> response =
                                            MessageEvents.getAllMessaging(clientGame.getNetwork(),
                                                    clientGame.getClan().getId()
                                            );
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            for (Message message : response.getData()) {
                                                String sender = "Notification";
                                                if (message.getUserId() != -1) {
                                                    sender = UserEvents.get(
                                                            clientGame.getNetwork(),
                                                            message.getUserId())
                                                            .getData().getUsername();
                                                }
                                                txtChat.setText(
                                                        txtChat.getText().toString() +
                                                                sender + " : "
                                                                + message.getMessage() + "\n"
                                                );
                                            }
                                            scrollView.removeAllViews();
                                            scrollView.addView(txtChat);
                                        }
                                    });
                                    sleep();
                                }
                            }

                            private void sleep() {
                                try {
                                    sleep(5000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                }
        );

        //SHOP
        Button btnClanShop = (Button) view.findViewById(R.id.btnClanShop);
        btnClanShop.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View content = getLayoutInflater().inflate(R.layout.layout_clan_shop, null);
                        Button btnBuyAttack = (Button) content.findViewById(R.id.btnBuyAttack);
                        btnBuyAttack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DataResponse<DecisionActionState> response = ActionEvents.buy(
                                        clientGame.getNetwork(),
                                        clientGame.getEngineState()
                                                .getUser().getId(),
                                        clientGame.getClan().getId(),
                                        1
                                );
                                if (response.getResponse().equals(CommunicationProtocol.OK))
                                    MessageUI.showToastMessage(_this, "The army purchase was put to a vote");
                            }
                        });
                        Button btnBuyDefense = (Button) content.findViewById(R.id.btnBuyDefense);
                        btnBuyDefense.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DataResponse<DecisionActionState> response =
                                        ActionEvents.buy(clientGame.getNetwork(),
                                                clientGame.getEngineState()
                                                        .getUser().getId(),
                                                clientGame.getClan().getId(),
                                                2
                                        );
                                if (response.getResponse().equals(CommunicationProtocol.OK))
                                    MessageUI.showToastMessage(_this, "The magic purchase was put to a vote");
                            }
                        });
                        popUpWindow = GameView.showAndGetPopUpWindow(
                                getLayoutInflater(), popUpWindow, fragment_menu, content, false
                        );
                    }
                }

        );
        //VOTING
        Button btnVoting = (Button) view.findViewById(R.id.btnVoting);
        btnVoting.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View content = getLayoutInflater().inflate(R.layout.layout_list_items, null);
                        LinearLayout linearLayout = (LinearLayout) content.findViewById(R.id.scroll_container);
                        DataResponse<DecisionActionState[]> response =
                                ActionEvents.getAllByUserClanActions(
                                        clientGame.getNetwork(),
                                        clientGame.getClan().getId(),
                                        clientGame.getEngineState().getUser().getId()
                                );
                        if (response.getData().length == 0)
                            MessageUI.showToastMessage(_this, "There are no active voting available " +
                                    "or you already vote");
                        else {
                            for (DecisionActionState decisionActionState : response.getData()) {
                                linearLayout.addView(getVotingView(decisionActionState, (ViewGroup) content));
                            }
                            popUpWindow = GameView.showAndGetPopUpWindow(
                                    getLayoutInflater(), popUpWindow, fragment_menu, content, true
                            );
                        }
                    }
                }

        );
        //REQUESTS
        Button btnClanGameRequests = (Button) view.findViewById(R.id.btnClanGameRequests);
        btnClanGameRequests.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View content = getLayoutInflater().inflate(R.layout.layout_list_items, null);
                        LinearLayout linearLayout = (LinearLayout) content.findViewById(R.id.scroll_container);
                        DataResponse<GameRequest[]> response = RequestEvents.getAllbyClanRequests(
                                clientGame.getNetwork(),
                                clientGame.getEngineState().getUser().getId(),
                                clientGame.getClan().getId()
                        );
                        if (response.getCode() == CommunicationProtocol.PERMISSION_DENIED_CODE) {
                            MessageUI.showToastMessage(_this, "You have to be the clan`s administrator " +
                                    "in order to perform this action");
                        } else if (response.getData().length == 0)
                            MessageUI.showToastMessage(_this, "You have no requests");
                        else {
                            for (GameRequest gameRequest : response.getData()) {
                                DataResponse<User> userResponse = UserEvents.get(
                                        clientGame.getNetwork(),
                                        gameRequest.getUserId());
                                linearLayout.addView(
                                        getRequestView(
                                                gameRequest, userResponse.getData(), (ViewGroup) content
                                        ));
                            }
                            popUpWindow = GameView.showAndGetPopUpWindow(
                                    getLayoutInflater(), popUpWindow, fragment_menu, content, true
                            );
                        }
                    }
                }

        );
    }


    public View getRequestView(
            final GameRequest gameRequest, User user, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.layout_clan_request, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.txtUsername);
        textView.setText(user.getUsername());
        Button btnAccept = (Button) rowView.findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestEvents.responseRequest(clientGame.getNetwork(),
                        gameRequest.getIdRequest(), true);
                AnimationUtil.fadeView(rowView);
            }
        });
        Button btnDecline = (Button) rowView.findViewById(R.id.btnDecline);
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestEvents.responseRequest(clientGame.getNetwork(),
                        gameRequest.getIdRequest(), false);
                AnimationUtil.fadeView(rowView);
            }
        });
        return rowView;
    }

    public View getVotingView(final DecisionActionState decisionActionState, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.layout_clan_vote, parent, false);

        final Chronometer voteChronometer = (Chronometer) rowView.findViewById(R.id.chronometer);
        voteChronometer.setBase(SystemClock.elapsedRealtime() - decisionActionState.getSeconds() * 1000);
        voteChronometer.setFocusable(false);
        voteChronometer.start();

        Button bntAccept = (Button) rowView.findViewById(R.id.btnAccept);
        bntAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionEvents.voteGameAction(
                        clientGame.getNetwork(),
                        decisionActionState.getActionId(),
                        clientGame.getEngineState().getUser().getId(),
                        true
                );
                AnimationUtil.fadeView(rowView);
            }
        });
        Button btnDecline = (Button) rowView.findViewById(R.id.btnDecline);
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActionEvents.voteGameAction(
                        clientGame.getNetwork(),
                        decisionActionState.getActionId(),
                        clientGame.getEngineState().getUser().getId(),
                        false
                );
                AnimationUtil.fadeView(rowView);
            }
        });
        ImageView imgView = (ImageView) rowView.findViewById(R.id.imgVotation);
        if (decisionActionState.getType() == 1) {
            //String resourceMessage = getResources().getString(R.string.resource_armor_detail);
            imgView.setImageResource(R.drawable.weapons);
        } else if (decisionActionState.getType() == 2) {
            //String resourceMessage = getResources().getString(R.string.resource_armor_detail);
            imgView.setImageResource(R.drawable.items);
        }
        return rowView;
    }

    public View getResourceView(final GameResource gameResource, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.layout_clan_resource, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.txtResourceDetails);
        ImageView imgView = (ImageView) rowView.findViewById(R.id.imgResource);
        if (gameResource.getType() == 0) {
            String resourceMessage = getResources().getString(R.string.resource_evil_eye_detail);
            textView.setText(
                    "EVIL EYE: " + resourceMessage + "\n" +
                            "Quantity available: " + gameResource.getQuantity()
            );
            imgView.setImageResource(R.drawable.evil_eye_copia);
        } else if (gameResource.getType() == 1) {
            String resourceMessage = getResources().getString(R.string.resource_armor_detail);
            textView.setText(
                    "ARMOR: " + resourceMessage + "\n" +
                            "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.armor_copia);
        } else if (gameResource.getType() == 2) {
            String resourceMessage = getResources().getString(R.string.resource_armor_detail);
            textView.setText(
                    "UNICORN: " + resourceMessage + "\n" +
                            "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.unicorn_copia);
        }
        return rowView;
    }

    ///*****************************************************
    //MAP
    private void mapListeners() {
        Button btnMapResources = (Button) findViewById(R.id.btnMapResources);
        btnMapResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markerType = 0;
                setUpAndUpdateMap();
            }
        });
        Button btnMapClan = (Button) findViewById(R.id.btnMapClan);
        btnMapClan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markerType = 1;
                setUpAndUpdateMap();
            }
        });
        Button btnMapMembers = (Button) findViewById(R.id.btnMapMembers);
        btnMapMembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markerType = 2;
                setUpAndUpdateMap();
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                switch (markerType) {
                    case 0:
                        showResourceMapWindow(marker);
                        break;
                    case 1:
                        showClanMapWindow(marker);
                        break;
                    case 2:
                        showClanMapWindow(marker);
                        break;
                }
                return true;
            }
        });
    }

    private void showResourceMapWindow(Marker marker) {
        final Activity _this = this;
        // Getting view from the layout file info_window_layout
        final GameResource gameResource =
                ResourcesSearch.getGameResourceById(Integer.valueOf(marker.getTitle()),
                        clientGame.getResources());
        View content = GameView.getResourcesMapWindow(gameResource,
                getLayoutInflater(), lastLocation);
        Button btnCollect = (Button) content.findViewById(R.id.btnCollect);
        btnCollect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Location location = mMap.getMyLocation();
                        if (clientGame.getClan().getId() == -1)
                            MessageUI.showToastMessage(_this,
                                    "You should create or register to a clan in " +
                                            "order to collect any resource.");
                        else if (location == null) {
                            MessageUI.showToastMessage(_this,
                                    "There is a problem getting you location, " +
                                            "try again in a few seconds");
                        } else if (!Util.distanceInsideRange(
                                new GeoPoint(location.getLatitude(),
                                        location.getLongitude()),
                                gameResource.getGeoPoint(),
                                1000
                        )) {
                            MessageUI.showToastMessage(_this,
                                    "You have to be at least at 1km of any resource " +
                                            "in order to collect it");
                        } else {
                            ResourcesEvents.collect(
                                    clientGame.getNetwork(),
                                    gameResource.getId(),
                                    clientGame.getEngineState().getUser().getId(),
                                    new GeoPoint(
                                            location.getLatitude(),
                                            location.getLongitude()
                                    ));
                            popUpWindow.dismiss();
                        }
                    }
                });
            }
        });
        popUpWindow = GameView.showAndGetPopUpWindow(
                getLayoutInflater(), popUpWindow, fragment_menu, content, true
        );
    }

    private void showClanMapWindow(Marker marker) {
        final Activity _this = this;
        final Clan clan = ClanSearch.getClanByName(marker.getTitle(),
                clientGame.getClans());
        View content = GameView.getClanMapWindow(clan,
                getLayoutInflater(), lastLocation);

        Button btnSendRequest = (Button) content.findViewById(R.id.btnSendRequest);
        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DataResponse<GameRequest> response =
                                RequestEvents.sendRequest(
                                        clientGame.getNetwork(),
                                        clan.getId(),
                                        clientGame.getEngineState().getUser().getId()
                                );
                        if (response.getCode() ==
                                CommunicationProtocol.ALREADY_IN_CODE)
                            MessageUI.showToastMessage(_this,
                                    "You are already a member of this clan");
                        else if (response.getCode() ==
                                CommunicationProtocol.ALREADY_CREATED_CODE)
                            MessageUI.showToastMessage(_this,
                                    "You already send a request to this clan");
                        else MessageUI.showToastMessage(_this, "Request sent successfully");
                    }
                });
            }
        });
        popUpWindow = GameView.showAndGetPopUpWindow(
                getLayoutInflater(), popUpWindow, fragment_menu, content, true
        );
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.setMyLocationEnabled(true);
                setUpAndUpdateMap();
                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    boolean first = true;

                    @Override
                    public void onMyLocationChange(Location googleLocation) {
                        lastLocation = googleLocation;
                        if (first)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(googleLocation.getLatitude(),
                                            googleLocation.getLongitude()), 10.0f));
                        first = false;
                        clientGame.getEngineState().getUser().setGeoPoint(
                                new GeoPoint(
                                        googleLocation.getLatitude(),
                                        googleLocation.getLongitude()
                                )
                        );
                        new Thread() {
                            @Override
                            public void run() {
                                UserEvents.update(
                                        clientGame.getNetwork(),
                                        clientGame.getEngineState().getUser().getGeoPoint());
                            }
                        }.start();
                    }
                });
                //confMap();
            }
        }
    }

    private void setUpAndUpdateMap() {
        mMap.clear();
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        switch (markerType) {
            case 0:
                GameData.loadResourcesMarkers(
                        clientGame, mMap,
                        this, mMap.getMyLocation());
                break;
            case 1:
                GameData.loadClansMarkers(clientGame, mMap, this);
                break;
        }
    }

    ///*****************************************************
    //OTHERS
    public GoogleMap getmMap() {
        return mMap;
    }

    public void setmMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    public ClientGame getClientGame() {
        return clientGame;
    }

    public void setClientGame(ClientGame clientGame) {
        this.clientGame = clientGame;
    }

    public PopupWindow getPopUpWindow() {
        return popUpWindow;
    }

    public void setPopUpWindow(PopupWindow popUpWindow) {
        this.popUpWindow = popUpWindow;
    }

    public int getMarkerType() {
        return markerType;
    }

    public void setMarkerType(int markerType) {
        this.markerType = markerType;
    }

    public FrameLayout getFragment_menu() {
        return fragment_menu;
    }

    public void setFragment_menu(FrameLayout fragment_menu) {
        this.fragment_menu = fragment_menu;
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initActivity();
    }
}

