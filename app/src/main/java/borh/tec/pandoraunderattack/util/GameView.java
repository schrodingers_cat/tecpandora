package borh.tec.pandoraunderattack.util;

import android.location.Location;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import bohr.engine.admin.GeoPoint;
import bohr.engine.util.Util;
import borh.tec.pandoraunderattack.R;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;

/**
 * Created by randall on 22/09/2015.
 */
public class GameView {
    public static View getClanMapWindow(Clan clan,
                                        LayoutInflater layoutInflater,
                                        Location location) {
        View clanInfoView = layoutInflater.inflate(R.layout.layout_map_clan, null);
        TextView textView = (TextView) clanInfoView.findViewById(R.id.txtClanDetails);
        String details = "Clan: " + clan.getClanName() + "\n" +
                "Owner: " + clan.getOwnerId() + "\n" +
                "Location: " + clan.getOwnerId() + "\n";
        if (location != null) {
            details += "Distance: " + Integer.toString((int) Util.distanceBetweenPoints(
                    new GeoPoint(location.getLatitude(),
                            location.getLongitude()),
                    clan.getGeoPoint()
            )) + "m";
        }
        textView.setText(details);
        return clanInfoView;
    }

    public static View getResourcesMapWindow(GameResource gameResource,
                                             LayoutInflater layoutInflater,
                                             Location location) {
        View resourceInfoView = layoutInflater.inflate(R.layout.layout_map_resource, null);
        TextView textView = (TextView) resourceInfoView.findViewById(R.id.txtResourceDetails);
        ImageView imgView = (ImageView) resourceInfoView.findViewById(R.id.imgResource);
        String distanceDetail = "";
        if (location != null)
            distanceDetail += "Distance: " + Integer.toString((int) Util.distanceBetweenPoints(
                    new GeoPoint(location.getLatitude(),
                            location.getLongitude()),
                    gameResource.getGeoPoint()
            )) + "m" + "\n";
        if (gameResource.getType() == 0) {
            textView.setText(
                    "EVIL EYE: \n" + "There is no sympathy for the devil.\n" + distanceDetail +
                            "Quantity available: " + gameResource.getQuantity()
            );
            imgView.setImageResource(R.drawable.evil_eye_copia);
        } else if (gameResource.getType() == 1) {
            textView.setText(
                    "ARMOR: \n" + "Without an army you will lose any battle.\n" + distanceDetail +
                            "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.armor_copia);
        } else {
            textView.setText(
                    "UNICORN: \n" + "Be creative or die on the hands of the creative one`s.\n" +
                            distanceDetail + "Quantity available: " + gameResource.getQuantity());
            imgView.setImageResource(R.drawable.unicorn_copia);
        }
        return resourceInfoView;
    }

    public static PopupWindow showAndGetPopUpWindow(LayoutInflater layoutInflater,
                                                    PopupWindow popupWindow,
                                                    View parent, View content, boolean scroll) {
        popupWindow.dismiss();
        //Window
        View pupupView = layoutInflater.inflate(
                R.layout.layout_scrollable, null);
        final PopupWindow newPopupWindow = new PopupWindow(pupupView);
        newPopupWindow.setFocusable(true);
        newPopupWindow.update();
        //Scroll
        if (scroll) {
            ScrollView container = (ScrollView) pupupView.findViewById(R.id.scroll_container);
            //Adding content to scroll_container
            container.addView(content);
        } else {
            LinearLayout container = (LinearLayout) pupupView.findViewById(R.id.linear_container);
            //Adding content to scroll_container
            container.addView(content);
        }
        //Evento para cerrar la ventana
        Button btnClose = (Button) pupupView.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newPopupWindow.dismiss();
            }
        });
        newPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        newPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        newPopupWindow.setContentView(pupupView);
        newPopupWindow.showAtLocation(parent, Gravity.NO_GRAVITY, 0, 10);
        return newPopupWindow;
    }

}
