package borh.tec.pandoraunderattack.util;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import bohr.engine.util.list.List;
import borh.tec.pandoraunderattack.R;
import borh.tec.pandoraunderattack.clan.MapsActivity;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.main.ClientGame;
import game.model.net.gameEvents.ClanEvents;
import game.model.net.gameEvents.ResourcesEvents;
import bohr.engine.net.protocol.DataResponse;

/**
 * Created by randall on 22/09/2015.
 */
public class GameData {
    public static void loadResourcesMarkers(final ClientGame clientGame,
                                            final GoogleMap mMap,
                                            final MapsActivity mapsActivity,
                                            final Location location
    ) {
        new Thread() {
            @Override
            public void run() {
                if (location != null) {
                    final DataResponse<GameResource[]> response =
                            ResourcesEvents.getAllResources(
                                    clientGame.getNetwork(),
                                    new GeoPoint(
                                            location.getLatitude(),
                                            location.getLongitude()
                                    ));
                    clientGame.setMapResources(new List(response.getData()));
                    mapsActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (GameResource gr : response.getData()) {
                                //String details = "Quantity: " + gr.getCost() + "\n";
                                MarkerOptions marker = new MarkerOptions().position(
                                        new LatLng(gr.getGeoPoint().getLatitude(),
                                                gr.getGeoPoint().getLongitude())
                                );
                                marker.title(String.valueOf(gr.getId()));
                                if (gr.getType() == 0) {
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.evil_eye));
                                } else if (gr.getType() == 1) {
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.armor));
                                } else if (gr.getType() == 2) {
                                    marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.unicorn));
                                }
                                mMap.addMarker(marker);
                            }
                        }
                    });
                }
            }
        }.start();
    }

    public static void loadClansMarkers(final ClientGame clientGame,
                                        final GoogleMap mMap,
                                        final MapsActivity mapsActivity) {
        new Thread() {
            @Override
            public void run() {
                final DataResponse<Clan[]> response =
                        ClanEvents.getAllClans(clientGame.getNetwork(), new GeoPoint());

                clientGame.setClans(new List(response.getData()));
                mapsActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (Clan clan : response.getData()) {
                            //String details = "Quantity: " + gr.getCost() + "\n";
                            MarkerOptions marker = new MarkerOptions().position(
                                    new LatLng(clan.getGeoPoint().getLatitude(),
                                            clan.getGeoPoint().getLongitude())
                            );
                            marker.title(String.valueOf(clan.getClanName()));
                            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.bloody_axe));
                            mMap.addMarker(marker);
                        }
                    }
                });
            }
        }.start();
    }
}
