package model.dao;

import java.util.Properties;

import bohr.engine.admin.User;
import bohr.engine.util.Util;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.others.GameItem;
import game.model.entities.others.GameResource;
import game.model.entities.others.Vote;
import game.model.entities.clan.Clan;
import game.model.threads.attackAction.AttackActionState;
import game.model.threads.decisionAction.DecisionActionState;
import game.model.threads.decisionAction.DecisionActionThread;
import game.model.util.ActionSearch;
import game.model.util.ClanSearch;
import game.model.util.UserSearch;
import model.dao.intefaces.ActionDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz ActionDAO para asi encargarse de manejar las acciones posibles en el protocolo de las acciones
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ActionDAOList implements ActionDAO {
    List<Clan> clans;
    List<User> users;
    List<DecisionActionThread> desicions;
    MessagesDAOList messagesDAOList;
    private int actionLastId;
    private int liderVote;
    private int coliderVote;
    private int puebloVote;
    private int esclavoVote;
    private int seconds;
    private int attacksInfo[][];

    /**
     * Constructor de la clase
     *
     * @param clans lista de los clanses
     * @param users lista de los usuarios
     */
    public ActionDAOList(List<Clan> clans, List<User> users, MessagesDAOList messagesDAOList) {
        actionLastId = 0;
        attacksInfo = new int[2][2];
        this.clans = clans;
        this.users = users;
        desicions = new List<>();
        this.messagesDAOList = messagesDAOList;
    }

    /**
     * Metodo encargado de comprar recursos de ataque
     *
     * @param idUser id del usuario
     * @param idClan id del clan.
     * @param type   tipo del recurso de ataque a comprar
     * @return respuesta del servidor; retorna la respuesta a la compra
     */
    @Override
    public DataResponse<DecisionActionState> buy(int idUser, int idClan, int type) {
        DataResponse<DecisionActionState> response =
                new DataResponse(new DecisionActionState());

        final GameItem aR = new GameItem(type, attacksInfo[type - 1][0], attacksInfo[type - 1][1]);
        final Clan clan = ClanSearch.getClanbyId(idClan, clans);
        User user = UserSearch.getUserById(idUser, users);
        if (clan == null || user == null || aR == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (aR.getCost() > clan.quantityResourceByType(type)) {
            response.setCode(CommunicationProtocol.NOT_ENOUGH_RESOURCES_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        actionLastId++;
        DecisionActionThread d = new DecisionActionThread(clan, actionLastId, type, liderVote,
                coliderVote, puebloVote, esclavoVote, seconds) {
            @Override
            protected void execAction(int type) {
                String msgAvailable = "Items available( ";
                if (type == 1) {
                    clan.getGameItems().add(aR);
                    GameResource gr = clan.updateResources(aR.getType(), -1 * aR.getCost());
                    msgAvailable += "ARMY=" + gr.getQuantity();
                    messagesDAOList.send(-1, clan.getId(), "You have a new ARMY item");
                } else if (type == 2) {
                    clan.getGameItems().add(aR);
                    GameResource gr = clan.updateResources(aR.getType(), -1 * aR.getCost());
                    msgAvailable += " MAGIC=" + gr.getQuantity();
                    messagesDAOList.send(-1, clan.getId(), "You have a new MAGIC item");
                }
                messagesDAOList.send(-1, clan.getId(), msgAvailable + " )");
                   /* System.out.println("EJECUTANDO EL TIPO " + type);
                    System.out.println(new Gson().toJson(clan));*/
            }
        };
        desicions.add(d);
        response.setData(d.getState());
        return response;
    }

    /**
     * Metodo encargado de la votacion de las acciones
     *
     * @param idUser         id del usuario
     * @param idAction       id de la accion.
     * @param actionResponse tipo de respuesta.
     * @return respuesta del servidor.
     */
    @Override
    public DataResponse<DecisionActionState> vote(int idAction, int idUser,
                                                  boolean actionResponse) {
        DataResponse<DecisionActionState> response =
                new DataResponse(new DecisionActionState());
        DecisionActionThread decisionAction =
                ActionSearch.getDecisionActionThreadById(idAction, desicions);
        User user = UserSearch.getUserById(idUser, users);
        if (user == null || decisionAction == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        Vote vote = ActionSearch.getVoteByUserID(
                idUser, decisionAction.getVotes());
        if (vote != null) {
            response.setCode(CommunicationProtocol.ALREADY_VOTED_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        decisionAction.getVotes().add(new Vote(idUser, actionResponse));
        response.setData(decisionAction.getState());
        return response;
    }

    /**
     * Metodo encargado de obtener los votos del clan
     *
     * @param idClan id del clan.
     * @return respuesta del servidor; retorna la lista de los votos.
     */
    @Override
    public DataResponse<DecisionActionState[]> getAllByUserClan(int idClan, int userId) {
        DataResponse<DecisionActionState[]> data =
                new DataResponse(new DecisionActionState[0]);
        List<DecisionActionState> notVotedList =
                ActionSearch.getDesicionActionByClanId(idClan, desicions);
        Node<DecisionActionThread> tmpDesicion = desicions.getHead();
        while (tmpDesicion != null) {
            boolean voted = false;
            Node<Vote> tmpVote = tmpDesicion.getData().getVotes().getHead();
            while (tmpVote != null) {
                if (tmpVote.getData().getUserId() == userId) {
                    voted = true;
                }
            }
            if (!voted) notVotedList.add(tmpDesicion.getData().getState());
        }
        data.setData(notVotedList.toArray());
        return data;
    }

    /**
     * Metodo encargado del ataque
     *
     * @param idUser id del usuario
     * @param idClan id del clan.
     * @return respuesta del servidor.
     */
    @Override
    public DataResponse<AttackActionState> pandoraAttack(int idUser, int idClan) {
        DataResponse<AttackActionState> data =
                new DataResponse(new AttackActionState());
        final Clan clanAttacked = ClanSearch.getClanbyId(idClan, clans);
        User userAttacking = UserSearch.getUserById(idUser, users);
        if (clanAttacked != null && userAttacking != null) {
            actionLastId++;
           /* AttackActionThread d = new AttackActionThread() {
                @Override
                protected void execAction(int type) {
                    if (type < 3) {
                        clan.getGameItems().add(aR);
                        clan.updateResources(aR.getType(), -1 * aR.getCost());
                    }
                    System.out.println("EJECUTANDO EL TIPO " + type);
                    System.out.println(new Gson().toJson(clan));
                }
            };
            desicions.add(d);
            data.setData(d.getState());
            */
            return data;
        }
        data.setCode(CommunicationProtocol.NOT_FOUND_CODE);
        return data;
    }

    /**
     * Método para establecer propiedades iniciales desde un objeto Properties
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        String strLider = properties.getProperty("liderVote");
        String strColider = properties.getProperty("coliderVote");
        String strPueblo = properties.getProperty("puebloVote");
        String strEsclavo = properties.getProperty("esclavoVote");
        String strTimeToVote = properties.getProperty("timeToVote");
        int liderV = 10;
        int coliderV = 5;
        int puebloV = 2;
        int esclavoV = 1;
        int seconds = 120;
        if (strTimeToVote != null && Util.isInteger(strTimeToVote)) {
            seconds = Integer.valueOf(strTimeToVote);
        }
        if (strLider != null && Util.isInteger(strLider)) {
            liderV = Integer.valueOf(strLider);
        }
        if (strColider != null && Util.isInteger(strColider)) {
            coliderV = Integer.valueOf(strColider);
        }
        if (strPueblo != null && Util.isInteger(strPueblo)) {
            puebloV = Integer.valueOf(strPueblo);
        }
        if (strEsclavo != null && Util.isInteger(strEsclavo)) {
            esclavoV = Integer.valueOf(strEsclavo);
        }

        this.seconds = seconds;
        this.liderVote = liderV;
        this.coliderVote = coliderV;
        this.puebloVote = puebloV;
        this.esclavoVote = esclavoV;

        String strWeaponCost = properties.getProperty("weaponCost");
        String strShieldCost = properties.getProperty("shieldCost");
        attacksInfo[0] = new int[]{1, 1500};
        attacksInfo[1] = new int[]{2, 3500};
        if (strWeaponCost != null && Util.isInteger(strWeaponCost)) {
            attacksInfo[0][1] = Integer.valueOf(strWeaponCost);
        }
        if (strShieldCost != null && Util.isInteger(strShieldCost)) {
            attacksInfo[1][1] = Integer.valueOf(strShieldCost);
        }
        System.out.println("liderVote=" + liderV);
        System.out.println("coliderVote=" + coliderV);
        System.out.println("puebloVote=" + puebloV);
        System.out.println("liderVote=" + liderV);
        System.out.println("esclavoVote=" + esclavoV);
        System.out.println("weaponCost=" + attacksInfo[0][1]);
        System.out.println("shieldCost=" + attacksInfo[1][1]);
        System.out.println("timeToVote=" + seconds);
    }

}
