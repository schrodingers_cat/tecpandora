package model.dao;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

import bohr.engine.admin.User;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import bohr.engine.util.observable.Observable;
import bohr.engine.util.observable.Observer;
import bohr.engine.admin.GeoPoint;
import game.model.entities.clan.Clan;
import game.model.entities.clan.ClanMember;
import game.model.entities.clan.Relic;
import game.model.util.ClanSearch;
import game.model.util.UserSearch;
import model.dao.intefaces.ClanDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz ClanDAO para asi encargarse de manejar las acciones posibles en el protocolo de los clanes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class ClanDAOList implements ClanDAO {
    private List<Clan> clans;
    private List<User> users;
    private List<Relic> relics;
    private int lastIdClan;

    /**
     * Constructor de la clase
     *
     * @param clans lista de los clanses
     */
    public ClanDAOList(List<Clan> clans, List<User> users, List<Relic> relics) {
        this.clans = clans;
        this.users = users;
        this.relics = relics;
        lastIdClan = 0;

    }

    /**
     * Metodo encargado de crear clanes
     *
     * @param gP    punto donde se pondra la reliquia
     * @param name  nombre del clan.
     * @param owner id del creador del clan
     * @param type  tipo de clan creado
     * @return respuesta del servidor; OK o Clan existente
     */
    @Override
    public DataResponse<Clan> create(String name, int type, GeoPoint gP, int owner) {
        DataResponse<Clan> data =
                new DataResponse(new Clan());
        if (ClanSearch.getClanByName(name, clans) == null) {
            lastIdClan++;
            Clan clan = new Clan(lastIdClan, type, name, owner, gP);
            clan.getMembers().add(new ClanMember(owner, 0, 1));
            relics.add(new Relic(gP, lastIdClan, clan));
            clans.add(clan);
            data.setData(clan);
            return data;
        }
        data.setCode(CommunicationProtocol.ALREADY_CREATED_CODE);
        return data;
    }

    @Override
    public DataResponse<Clan> login(int idUser, String clanName) {
        DataResponse<Clan> response =
                new DataResponse(new Clan());
        Clan clan = ClanSearch.getClanByName(clanName, clans);
        User user = UserSearch.getUserById(idUser, users);
        if (clan == null || user == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        ClanMember clanMember = ClanSearch.getClanByMemberId(idUser, clan);
        if (clanMember == null) {
            response.setCode(CommunicationProtocol.NOT_A_MEMBER_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        response.setData(clan);
        return response;
    }

    @Override
    public DataResponse<Clan> get(String name) {
        DataResponse<Clan> response =
                new DataResponse(new Clan());
        Clan clan = ClanSearch.getClanByName(name, clans);
        if (clan == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        response.setData(clan);
        return response;
    }

    /**
     * Metodo encargado de hacer una lista con todos los clanes creados
     *
     * @return lista con todos los clanes creados
     */
    @Override
    public DataResponse<Clan[]> getAll() {
        DataResponse<Clan[]> all =
                new DataResponse(new Clan[0]);
        all.setData(clans.toArray());
        return all;
    }


    @Override
    /**
     * Este metodo mantiene la comunicación se mantiene escuchando y leyendo las acciones
     */
    public void listen(final Socket playerSocket, final BufferedReader in,
                       final PrintWriter out, int userId) {
        Node<Clan> tmpClanNode = clans.getHead();
        while (tmpClanNode != null) {
            Clan clan = tmpClanNode.getData();
            if (clan.getOwnerId() == userId) {
                addObserverToClan(clan, playerSocket, in, out);
            }
            Node<ClanMember> tmpClanMemberNode = clan.getMembers().getHead();
            while (tmpClanMemberNode != null) {
                if (tmpClanMemberNode.getData().getIdUser() == userId)
                    addObserverToClan(clan, playerSocket, in, out);
            }
            tmpClanNode = tmpClanNode.getNextNode();
        }
    }

    private void addObserverToClan(final Clan clan, final Socket playerSocket,
                                   final BufferedReader in, final PrintWriter out) {
        clan.addObserver(new Observer() {
            @Override
            public void update(Observable obs, Object msg) {
                if (!playerSocket.isClosed()) {
                    out.println((String) msg);
                } else {
                    synchronized (clan) {
                        clan.deleteObserver(this);
                    }
                }
            }
        });
    }
}
