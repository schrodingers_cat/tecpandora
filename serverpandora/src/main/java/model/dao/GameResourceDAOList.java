package model.dao;


import com.google.gson.GsonBuilder;

import java.util.Properties;

import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.util.Util;
import bohr.engine.admin.User;
import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameResource;
import bohr.engine.admin.GeoPoint;
import game.model.net.GameResourceRequest;
import game.model.threads.CollectActionThread;
///import model.dao.intefaces.DAOResponse;
import game.model.util.ClanSearch;
import game.model.util.ResourcesSearch;
import game.model.util.UserSearch;
import model.dao.intefaces.GameResourceDAO;
import bohr.engine.net.protocol.DataResponse;

/**
 * Clase que usa los metodos de la interfaz GameResourceDAO para asi encargarse de manejar las acciones posibles en el protocolo de las recursos
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameResourceDAOList implements GameResourceDAO {
    private List<GameResource> resources;
    private List<CollectActionThread> collectActionThreads;
    private List<User> players;
    private List<Clan> clans;
    private double radioRangeCollect;
    private double radioRangeView;

    /**
     * Constructor de la clase
     *
     * @param clans   lista de los clanses
     * @param players lista de los jugadores
     */
    public GameResourceDAOList(List<Clan> clans, List<User> players,
                               List<GameResource> resources) {
        this.clans = clans;
        this.players = players;
        this.resources = resources;
        collectActionThreads = new List<>();
        radioRangeCollect = (double) 1000;
        radioRangeView = (double) 20000;
    }

    /**
     * Metodo encargado de obtener todos los recursos
     *
     * @param geoPoint punto en el mapa.
     * @return respuesta del servidor; retorna todos los recursos
     */
    @Override
    public DataResponse<GameResource[]> getAll(GeoPoint geoPoint) {
        DataResponse<GameResource[]> all =
                new DataResponse(new GameResource[0]);
        //System.out.print(_resources.size());
        synchronized (resources) {
            List<GameResource> resourceList = new List<>();
            Node<GameResource> tmpResourceNode = resources.getHead();
            while (tmpResourceNode != null) {
                if (Util.distanceInsideRange(
                        geoPoint,
                        tmpResourceNode.getData().getGeoPoint(),
                        radioRangeView))
                    resourceList.add(tmpResourceNode.getData());
                tmpResourceNode = tmpResourceNode.getNextNode();
            }
            all.setData(resourceList.toArray());
            //all.setData(resources.toArray());
        }
        return all;
    }

    /**
     * Metodo encargado de obtener todos los recursos de un clan
     *
     * @param clanId id del clan
     * @return respuesta del servidor; retorna todos los recursos
     */
    @Override
    public DataResponse<GameResource[]> getAllByClan(int clanId) {
        DataResponse<GameResource[]> all =
                new DataResponse(new GameResource[0]);
        Clan clan = ClanSearch.getClanbyId(clanId, clans);
        if (clan != null)
            all.setData(clan.getResources().toArray());
        return all;
    }

    /**
     * Metodo encargado de recolectar recursos
     *
     * @param idResource id del recurso.
     * @param userId     id del usuario.
     * @param geoPoint   punto en el mapa.
     * @return respuesta del servidor; retorna la cantidad recolectada.
     */
    @Override
    public DataResponse<GameResourceRequest> collect(int idResource, int userId, GeoPoint geoPoint) {
        DataResponse<GameResourceRequest> response =
                new DataResponse(new GameResourceRequest());
        CollectActionThread collT =
                ResourcesSearch.getCollectActionThreadByResource(
                        idResource, collectActionThreads);
        GameResource gR = ResourcesSearch.getGameResourceById(idResource, resources);
        User user = UserSearch.getUserById(userId, players);
        if (gR == null || user == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (!Util.distanceInsideRange(gR.getGeoPoint(), geoPoint, radioRangeCollect)) {
            response.setCode(CommunicationProtocol.NOT_INSIDE_VALID_RANGE_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (collT != null) {
            User userSearch = UserSearch.getUserById(userId, collT.getUsersCollecting());
            //Si no está recolectando
            if (userSearch == null)
                collT.getUsersCollecting().add(userSearch);
            response.setData(new GameResourceRequest(
                    collT.getResource(), userId, -1, geoPoint));
        } else {
            collT = new CollectActionThread(gR, clans, radioRangeCollect, resources);
            collT.getUsersCollecting().add(user);
            collectActionThreads.add(collT);
            response.setData(new GameResourceRequest(
                    gR, userId, -1, geoPoint));
            collT.getCollectThread().start();
        }
        return response;
    }


    /**
     * Método para establecer propiedades iniciales desde un objeto Properties
     *
     * @param properties
     */
    public void setProperties(Properties properties) {
        String strRadioRangeCollect = properties.getProperty("radioRangeCollectResource");
        String strRadioRangeView = properties.getProperty("radioRangeViewResource");
        if (strRadioRangeCollect != null && Util.isDouble(strRadioRangeCollect)) {
            radioRangeCollect = Float.valueOf(strRadioRangeCollect);
        }
        if (strRadioRangeView != null && Util.isDouble(strRadioRangeView)) {
            radioRangeView = Float.valueOf(strRadioRangeView);
        }
        System.out.println("radioRangeCollect=" + radioRangeCollect);
        System.out.println("radioRangeView=" + radioRangeView);
    }
}
