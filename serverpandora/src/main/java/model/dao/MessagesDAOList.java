package model.dao;

import bohr.engine.util.list.List;
import game.model.entities.clan.Clan;
import game.model.entities.others.Message;
import bohr.engine.net.protocol.DataResponse;
import game.model.util.ClanSearch;
import model.dao.intefaces.MessagesDAO;

/**
 * Clase que usa los metodos de la interfaz MessagesDAOList para asi encargarse de manejar las acciones posibles en el protocolo de los mensajes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class MessagesDAOList implements MessagesDAO {
    private List<Clan> clans;
    private int lastId;

    /**
     * Constructor de la clase
     *
     * @param clans lista de los clanses
     */
    public MessagesDAOList(List<Clan> clans) {
        lastId = 0;
        this.clans = clans;
    }

    /**
     * Metodo encargado de los mensajes del clan
     *
     * @param clanId id del clan.
     * @return respuesta del servidor; retorna los mensajes para el clan
     */
    @Override
    public DataResponse<Message[]> getAll(int clanId) {
        DataResponse<Message[]> data =
                new DataResponse(new Message[0]);

        Clan clan = ClanSearch.getClanbyId(clanId, clans);
        if (clan != null) {
            data.setData(clan.getMessages().toArray());
        }
        return data;
    }

    /**
     * Metodo encargado de enviar mensajes del clan
     *
     * @param clanId id del clan.
     * @param userId id del usuario.
     * @param msg    mensaje a enviar.
     * @return respuesta del servidor; retorna el mensaje enviado
     */
    @Override
    public DataResponse<Message> send(int userId, int clanId, String msg) {
        DataResponse<Message> data =
                new DataResponse(new Message());
        lastId++;
        Message message = new Message(lastId, userId, clanId, msg);
        Clan clan = ClanSearch.getClanbyId(clanId, clans);
        if (clan != null) {
            clan.getMessages().add(message);
            data.setData(message);
        }
        return data;
    }
}
