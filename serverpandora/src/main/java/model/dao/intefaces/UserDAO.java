package model.dao.intefaces;

import java.net.Socket;
import java.util.ArrayList;

import bohr.engine.admin.User;
import bohr.engine.admin.UserRequest;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.util.list.List;

public interface UserDAO {
    public DataResponse<User[]> getAll();

    public DataResponse<User> signUp(Socket socket, String username, String password);

    public DataResponse<User> login(Socket socket, String username, String password);

    public DataResponse<User> get(int userId);

    public DataResponse<User> update(Socket socket, UserRequest userRequest);

    /*
        public JSONObject messageClients(String msg);

        public JSONObject messageClient(String msg, int sessionID);
    */
    public void listen(int idPlayer);

}
