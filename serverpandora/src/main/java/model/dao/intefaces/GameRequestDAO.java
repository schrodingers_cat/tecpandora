package model.dao.intefaces;

/**
 * Created by acacia on 08/09/15.
 */

import java.util.ArrayList;
import game.model.entities.others.GameRequest;
import bohr.engine.net.protocol.DataResponse;

public interface GameRequestDAO {

    public DataResponse<GameRequest> send(int clan, int User);

    public DataResponse<GameRequest> response(boolean accepted, int request);

    public DataResponse<GameRequest[]> getAllbyUser(int user);

    public DataResponse<GameRequest[]> getAllByClan(int clan, int userId);
}