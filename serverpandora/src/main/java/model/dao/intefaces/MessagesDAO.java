package model.dao.intefaces;

import java.util.ArrayList;

import bohr.engine.util.list.List;
import game.model.entities.others.Message;
import bohr.engine.net.protocol.DataResponse;

/**
 * Created by curso on 22/09/2015.
 */
public interface MessagesDAO {

    public DataResponse<Message[]> getAll(int idClan);

    public DataResponse<Message> send(int userId, int clanId, String msg);
}
