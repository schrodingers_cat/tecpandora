package model.dao.intefaces;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import bohr.engine.util.list.List;
import game.model.entities.clan.Clan;
import bohr.engine.admin.GeoPoint;
import bohr.engine.net.protocol.DataResponse;

public interface ClanDAO {
    public DataResponse<Clan[]> getAll();

    public DataResponse<Clan> create(String name, int type, GeoPoint geoPoint, int owner);

    public DataResponse<Clan> get(String name);

    public DataResponse<Clan> login(int idUser, String clanName);

    //public
    public void listen(Socket clientSocket, BufferedReader in, PrintWriter out, int idPlayer);
}
