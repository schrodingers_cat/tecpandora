package model.dao;

import java.net.Socket;

import bohr.engine.admin.User;
import bohr.engine.admin.UserRequest;
import bohr.engine.util.list.List;
import game.model.util.UserSearch;
import model.dao.intefaces.UserDAO;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataResponse;

/**
 * Clase que usa los metodos de la interfaz UserDAOList para asi encargarse de manejar las acciones posibles en el protocolo de los usuarios
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class UserDAOList implements UserDAO {
    private List<User> users;
    private int lastId;

    /**
     * Constructor de la clase
     *
     * @param players lista de los jugadores
     */
    public UserDAOList(List<User> players) {
        this.users = players;
        lastId = 0;
    }

    /**
     * Metodo encargado de registrar un jugador
     *
     * @param username nombre de usuario del jugador.
     * @param socket   .
     * @param password contraseña del jugador.
     * @return respuesta del servidor; retorna el usuario creado o ya hay un usuario creado con ese username y password.
     */
    @Override
    public DataResponse<User> signUp(Socket socket, String username, String password) {
        DataResponse<User> response = new DataResponse(new User());
        if (UserSearch.getPlayerByUserPassword(username, password, users) == null) {
            lastId++;
            User user = new User(lastId, socket, username, password);
            users.add(user);
            response.setData(user);
            return response;
        }
        response.setCode(CommunicationProtocol.ALREADY_CREATED_CODE);
        return response;
    }

    /**
     * Metodo encargado de iniciar un jugador
     *
     * @param username nombre de usuario del jugador.
     * @param socket   .
     * @param password contraseña del jugador.
     * @return respuesta del servidor; retorna el usuario o error.
     */
    @Override
    public DataResponse<User> login(Socket socket, String username, String password) {
        DataResponse<User> response = new DataResponse(new User());
        User user = null;
        if ((user = UserSearch.getPlayerByUserPassword(username, password, users)) != null) {
            user.setSocket(socket);
            response.setData(user);
            return response;
        }
        response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
        return response;
    }

    /**
     * Metodo encargado de iniciar un jugador
     *
     * @param id id de usuario.
     * @return respuesta del servidor; retorna el usuario con el id de entrada.
     */
    @Override
    public DataResponse<User> get(int id) {
        DataResponse<User> response = new DataResponse(new User());
        User user = UserSearch.getUserById(id, users);
        if (user != null)
            response.setData(user);
        return response;
    }

    @Override
    public DataResponse<User> update(Socket socket, UserRequest userRequest) {
        DataResponse<User> response = new DataResponse(new User());
        User user = UserSearch.getUserById(userRequest.getId(), users);
        if (user != null) {
            user.setSocket(socket);
            user.setGeoPoint(userRequest.getGeoPoint());
            response.setData(user);
        }
        return response;
    }

    /**
     * Metodo encargado de obtener todos los usuarios
     *
     * @return respuesta del servidor; retorna usuarios.
     */
    @Override
    public DataResponse<User[]> getAll() {
        DataResponse<User[]> response =
                new DataResponse(new User[0]);
        response.setData(users.toArray());
        return response;
    }

    /**
     * Metodo encargado de
     *
     * @param idPlayer id de jugador.
     * @return respuesta del servidor;
     */
    @Override
    public void listen(int idPlayer) {

    }


}
