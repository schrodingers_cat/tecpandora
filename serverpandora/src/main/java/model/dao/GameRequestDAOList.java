package model.dao;

import bohr.engine.admin.User;
import bohr.engine.util.list.List;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.entities.clan.ClanMember;
import game.model.util.ClanSearch;
import game.model.util.RequestSearch;
import game.model.util.UserSearch;
import model.dao.intefaces.GameRequestDAO;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.net.protocol.CommunicationProtocol;

/**
 * Clase que usa los metodos de la interfaz GameRequestDAO para asi encargarse de manejar las acciones posibles en el protocolo de las solicitudes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class GameRequestDAOList implements GameRequestDAO {
    private MessagesDAOList messagesDAOList;
    private List<GameRequest> requests;
    private List<Clan> clans;
    private List<User> users;
    private int lastID;

    /**
     * Constructor de la clase
     *
     * @param clans    lista de los clanes existentes
     * @param users    lista de los usuarios existentes
     * @param requests lista de las solicitudes existentes
     */
    public GameRequestDAOList(List<GameRequest> requests,
                              List<User> users, List<Clan> clans,
                              MessagesDAOList messagesDAOList) {
        this.messagesDAOList = messagesDAOList;
        this.requests = requests;
        this.clans = clans;
        this.users = users;
        lastID = 0;
    }

    /**
     * Metodo encargado de crear solicitudes de un usuario a un clan
     *
     * @param clanId iD de clan al que se le hace la solicitud
     * @param userId id del usuario que hace la solicitud
     * @return respuesta del servidor; OK o Solicitud existente
     */
    public DataResponse<GameRequest> send(int clanId, int userId) {
        DataResponse<GameRequest> response =
                new DataResponse(new GameRequest());
        Clan clan = ClanSearch.getClanbyId(clanId, clans);
        User user = UserSearch.getUserById(userId, users);
        if (clan == null || user == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (ClanSearch.getClanByMemberId(userId, clan) != null) {
            response.setCode(CommunicationProtocol.ALREADY_IN_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (RequestSearch.getRequestByUserClan(clanId, userId, requests) != null) {
            response.setCode(CommunicationProtocol.ALREADY_CREATED_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        lastID++;
        GameRequest gameRequest = new GameRequest(clanId, userId, false, lastID);
        requests.add(gameRequest);
        response.setData(gameRequest);
        return response;
    }

    /**
     * Metodo encargado de hacer una lista con las solicitudes hechas por un usuario
     *
     * @param iDuser id del usuario que ha enviado las solicitudes
     * @return lista con las solicitudes hechas por un usuario
     */
    @Override
    public DataResponse<GameRequest[]> getAllbyUser(int iDuser) {
        DataResponse<GameRequest[]> all =
                new DataResponse(new GameRequest[0]);
        all.setData(RequestSearch.getRequestByUser(iDuser, requests).toArray());
        return all;
    }

    /**
     * Metodo encargado de hacer una lista con las solicitudes recibidas por un clan
     *
     * @param clanId id del clan que quiere saber las solicitudes
     * @param userId id del user que quiere saber las solicitudes
     * @return lista con las solicitudes recibidas por un clan
     */
    @Override
    public DataResponse<GameRequest[]> getAllByClan(int clanId, int userId) {
        DataResponse<GameRequest[]> response =
                new DataResponse(new GameRequest[0]);
        Clan clan = ClanSearch.getClanbyId(clanId, clans);
        User user = UserSearch.getUserById(userId, users);
        if (clan == null || user == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        if (clan.getOwnerId() != user.getId()) {
            response.setCode(CommunicationProtocol.PERMISSION_DENIED_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        response.setData(RequestSearch.getActiveRequestByClan(
                clanId, requests).toArray());
        return response;
    }

    /**
     * Metodo encargado de responder las solicitudes
     *
     * @param requestResponse respuesta a la solicitud
     * @param requestId       numero de solicitud
     * @return
     */
    @Override
    public DataResponse<GameRequest> response(boolean requestResponse, int requestId) {
        DataResponse<GameRequest> response =
                new DataResponse(new GameRequest());
        GameRequest gameRequest = RequestSearch.getRequestById(requestId, requests);
        if (gameRequest == null) {
            response.setCode(CommunicationProtocol.NOT_FOUND_CODE);
            response.setResponse(CommunicationProtocol.ERROR);
            return response;
        }
        gameRequest.setResponse(requestResponse);
        gameRequest.setActive(false);
        response.setData(gameRequest);
        //Si respondieron que sí, agrega el usuario a los miembros
        if (gameRequest.positiveResponse()) {
            Clan clan = ClanSearch.getClanbyId(
                    gameRequest.getClanId(), clans);
            User user = UserSearch.getUserById(
                    gameRequest.getUserId(), users);
            clan.getMembers().add(
                    new ClanMember(gameRequest.getUserId(), 0, 4));
            messagesDAOList.send(
                    -1, clan.getId(),
                    "User " + user.getUsername() + " is a new member of this clan"
            );
        }
        return response;
    }


}