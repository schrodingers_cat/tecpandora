package model.socket;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import bohr.engine.net.protocol.DataResponse;
import bohr.engine.util.Util;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.Request;

/**
 * Clase con funcionalidad de server multi-hilo
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */

public abstract class ThreadedSocket extends Thread {
    private ServerSocket serverSocket;
    private boolean running;
    private int port;

    public ThreadedSocket(Properties properties) throws IOException {
        port = 8080;
        setProperties(properties);
        serverSocket = new ServerSocket(port);
        running = true;
    }

    /**
     * Método encargado de iniciar el hilo principal del server y además crea un hilo por cliente entrante
     */
    @Override
    public void run() {
        System.out.println("Waiting requests on port " + serverSocket.getLocalPort());
        while (running) {
            try {
                final Socket clientIn = serverSocket.accept();
                new Thread() {
                    @Override
                    public void run() {
                        initConection(clientIn);
                    }
                }.start();

            } catch (IOException | NullPointerException e) {
                System.out.println("SERVER SOCKET ERROR: " + e);
            }
        }
    }

    /**
     * Método encargado abrir y cerrar las conexiónes con el cliente
     *
     * @param client Socket que representa al cliente entrante
     */
    public void initConection(Socket client) {
        PrintWriter out = null;
        BufferedReader in = null;
        String requestJson = "";
        try {
            out = new PrintWriter(client.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            //CUIDADO!!!!!!!!!!!!!!!!!
            //**DEBE PERDURAR CONEXION HASTA QUE LLEGUE UN EXIT

            Request request = null;
            boolean connection = true;
            while (connection && (request = (new Gson().fromJson(
                    (
                            requestJson = in.readLine()
                    ),
                    Request.class))) != null) {
                if (!request.getType().equals(CommunicationProtocol.EXIT))
                    initProtocol(client, in, out, requestJson, request);
                else {
                    out.println(new Gson().toJson(
                            new DataResponse(CommunicationProtocol.OK,
                                    CommunicationProtocol.OK_CODE)));
                    connection = false;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("ERROR READING PROTOCOL: " + e);
            System.out.println("ERROR READING MESSAGE: (" + client.getInetAddress()
                            + ":" + client.getPort() + "): " +
                            requestJson
            );
        }
        try {
            if (in != null)
                in.close();
            if (out != null)
                out.close();
            if (client != null)
                client.close();
        } catch (Exception e) {
            System.out.println("ERROR CLOSING CONNECTION: " + e);
        }
    }

    /**
     * Método abstracto que debe implementar la lógica para responder al cliente
     *
     * @param client Socket que representa al cliente entrante
     * @param in     Objeto para la lectura desde el Socket cliente
     * @param out    Objeto para la escritura hacia el Socket cliente
     */
    protected abstract void initProtocol(Socket client, BufferedReader in,
                                         PrintWriter out, String requestJson,
                                         Request request)
            throws IOException;


    /**
     * Método para saber si el server está corriendo
     *
     * @return True si está corriendo; False si no está corriendo
     */
    public boolean isRunning() {
        return running;
    }

    public void setIsRun(boolean isRun) {
        this.running = isRun;
    }

    /**
     * Método para saber si el puerto server está corriendo
     *
     * @return True si está corriendo; False si no está corriendo
     */
    public int getPort() {
        return port;
    }

    /**
     * Método para establecer propiedades iniciales desde un objeto Properties
     *
     * @param properties
     */
    private void setProperties(Properties properties) {
        String strPort = properties.getProperty("port");
        if (strPort != null && Util.isInteger(strPort)) {
            setPort(Integer.valueOf(strPort));
        }
        System.out.println("port=" + getPort());
    }

    public void setPort(int port) {
        this.port = port;
    }


}
