package model.socket;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Properties;

import bohr.engine.admin.UserRequest;
import game.model.entities.clan.Clan;
import game.model.entities.others.Message;
import game.model.net.GameAction;
import game.model.entities.others.GameRequest;
import model.dao.ActionDAOList;
import model.dao.MessagesDAOList;
import model.dao.intefaces.*;
import game.model.net.GameResourceRequest;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.Request;
import bohr.engine.net.protocol.CommunicationProtocol;
import bohr.engine.net.protocol.DataResponse;

/**
 * Clase encargado de los socket encargados del flujo de datos entre el servidor y el juego.
 * Envia los DAO
 *
 * @author Randall Alfaro
 * @author Kevin Arse
 * @author Luis Fernando
 * @author Isaac Trejos
 */
public class SocketAdmin extends ThreadedSocket {
    private final UserDAO usersData;
    private final ClanDAO clanData;
    private final GameResourceDAO resourceData;
    private final GameRequestDAO requestData;
    private final ActionDAO actionData;
    private final MessagesDAO messagesData;

    /**
     * Constructor de los socket
     *
     * @param usersData    Datos de los usuarios
     * @param clanData     Datos de los clanes
     * @param resourceData Datos de los recursos
     * @param requestData  Datos de las solicitudes
     * @param actionData   Datos de las acciones realizadas
     * @param messagesData Datos de los mensajes
     * @param properties   Propiedades de las funciones
     * @throws IOException
     */
    public SocketAdmin(final UserDAO usersData, final ClanDAO clanData,
                       final GameResourceDAO resourceData, final GameRequestDAO requestData,
                       final ActionDAO actionData, final MessagesDAO messagesData,
                       Properties properties)
            throws IOException {
        super(properties);
        this.actionData = actionData;
        this.usersData = usersData;
        this.clanData = clanData;
        this.resourceData = resourceData;
        this.requestData = requestData;
        this.messagesData = messagesData;
    }

    @Override
    /**
     * Metodo encargado de la inicializacion de los protocolos del juego
     */
    protected void initProtocol(Socket client, BufferedReader in, PrintWriter out,
                                String requestJson, Request request) throws IOException {
        System.out.println("MESSAGE: (" + client.getInetAddress()
                        + ":" + client.getPort() + "): " +
                        requestJson
        );
        if (request != null) {
            response(client, out, requestJson, request);
        } else {
            out.println(new Gson().toJson(
                    new DataResponse(CommunicationProtocol.ERROR,
                            CommunicationProtocol.ERROR_CODE)));
        }
    }

    /**
     * Metodo encargado de las respuestas del servidor al juego, la comunicación con el usuario
     *
     * @param client      cliente que inicio el socket de conexión
     * @param out         Salida del mensaje
     * @param requestJson Petición del Json
     * @param request     Solicitudes enviadas
     */
    private void response(Socket client, PrintWriter out, String requestJson, Request request) {
        switch (request.getType()) {
            case CommunicationProtocol.USER_SERVICE:
                responseUserService(client, out, requestJson);
                break;
            case CommunicationProtocol.CLAN_SERVICE:
                responseClanService(client, out, requestJson);
                break;
            case CommunicationProtocol.GAME_RESOURCE_SERVICE:
                responseResourceService(client, out, requestJson);
                break;
            case CommunicationProtocol.GAME_REQUESTS_SERVICE:
                responseRequestService(client, out, requestJson);
                break;
            case CommunicationProtocol.GAME_ACTION_SERVICE:
                responseGameActionService(client, out, requestJson);
                break;
            case CommunicationProtocol.GAME_MESSAGING_SERVICE:
                responseGameMessagingService(client, out, requestJson);
                break;
        }

    }

    /**
     * Comunicacion entre usuario-servidor
     *
     * @param client      Cliente de juego
     * @param out         Salida
     * @param requestJson Solicitud de Gson
     */
    private void responseGameMessagingService(Socket client, PrintWriter out, String requestJson) {
        DataRequest<Message> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<Message>>() {
                        }.getType());
        switch (request.getAction()) {
            case CommunicationProtocol.SEND:
                out.println(new Gson().toJson(messagesData.send(
                        request.getData().getUserId(),
                        request.getData().getClanId(),
                        request.getData().getMessage()
                )));
                break;
            case CommunicationProtocol.GET_ALL:
                out.println(new Gson().toJson(messagesData.getAll(
                        request.getData().getClanId()
                )));
                break;
        }
    }

    /**
     * Comunicacion para los servicion de accion del juego
     *
     * @param client
     * @param out
     * @param requestJson
     */
    private void responseGameActionService(Socket client, PrintWriter out, String requestJson) {
        DataRequest<GameAction> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<GameAction>>() {
                        }.getType());
        switch (request.getAction()) {
            case CommunicationProtocol.BUY:
                out.println(new Gson().toJson(actionData.buy(
                        request.getData().getIdUser(),
                        request.getData().getIdClan(),
                        request.getData().getType()
                )));
                break;
            case CommunicationProtocol.GET_ALL:
                out.println(new Gson().toJson(actionData.getAllByUserClan(
                        request.getData().getIdClan(),
                        request.getData().getIdUser()
                )));
                break;
            case CommunicationProtocol.RESPONSE:
                out.println(new Gson().toJson(actionData.vote(
                        request.getData().getIdDecisionAction(),
                        request.getData().getIdUser(),
                        request.getData().isResponse()
                )));
                break;
        }
    }

    /**
     * Comunicacion de las peticiones de servicion del clan
     *
     * @param client
     * @param out
     * @param requestJson
     */
    private void responseRequestService(Socket client, PrintWriter out,
                                        String requestJson) {
        DataRequest<GameRequest> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<GameRequest>>() {
                        }.getType());

        switch (request.getAction()) {
            case CommunicationProtocol.SEND:
                out.println(new Gson().toJson(requestData.send(
                        request.getData().getClanId(), request.getData().getUserId())));
                break;
            case CommunicationProtocol.GET_ALL_BY_CLAN:
                out.println(new Gson().toJson(requestData.getAllByClan(
                        request.getData().getClanId(),
                        request.getData().getUserId()
                )));
                break;
            case CommunicationProtocol.GET_ALL_BY_USER:
                out.println(new Gson().toJson(requestData.getAllbyUser(
                        request.getData().getUserId()
                )));
                break;
            case CommunicationProtocol.RESPONSE:
                out.println(new Gson().toJson(requestData.response(
                                request.getData().positiveResponse(),
                                request.getData().getIdRequest()
                        )
                ));
                break;
        }
    }

    /**
     * Comunicacion que permite los servicios del usuario
     *
     * @param client
     * @param out
     * @param requestJson
     */
    private void responseUserService(Socket client, PrintWriter out,
                                     String requestJson) {
        DataRequest<UserRequest> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<UserRequest>>() {
                        }.getType());
        switch (request.getAction()) {
            case CommunicationProtocol.SIGN_UP: {
                out.println(new Gson().toJson(usersData.signUp(client,
                        request.getData().getUsername(),
                        request.getData().getPassword())));
                break;
            }
            case CommunicationProtocol.LOGIN: {
                out.println(new Gson().toJson(usersData.login(client,
                        request.getData().getUsername(),
                        request.getData().getPassword())));
                break;
            }
            case CommunicationProtocol.GET_ALL: {
                out.println(new Gson().toJson(usersData.getAll()));
                break;
            }
            case CommunicationProtocol.UPDATE: {
                out.println(new Gson().toJson(usersData.update(
                        client, request.getData()
                )));
                break;
            }
            case CommunicationProtocol.GET: {
                out.println(new Gson().toJson(usersData.get(
                        request.getData().getId()
                )));
                break;
            }
        }
    }

    /**
     * Comunicacion que permite los servicios del clan
     *
     * @param cliente
     * @param out
     * @param requestJson
     */
    private void responseClanService(Socket cliente, PrintWriter out,
                                     String requestJson) {
        DataRequest<Clan> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<Clan>>() {
                        }.getType());
        switch (request.getAction()) {
            case CommunicationProtocol.CREATE:
                out.println(new Gson().toJson(
                        clanData.create(
                                request.getData().getClanName(),
                                request.getData().getType(),
                                request.getData().getGeoPoint(),
                                request.getData().getOwnerId())));
                break;
            case CommunicationProtocol.GET_ALL:
                out.println(new Gson().toJson(clanData.getAll()));
                break;
            case CommunicationProtocol.GET:
                out.println(new Gson().toJson(
                        clanData.get(request.getData().getClanName())));
                break;
            case CommunicationProtocol.LOGIN:
                out.println(new Gson().toJson(
                        clanData.login(
                                request.getData().getOwnerId(),
                                request.getData().getClanName())));
                break;
        }
    }

    /**
     * Comunicacion que permite el servicion de los recursos
     *
     * @param client
     * @param out
     * @param requestJson
     */
    private void responseResourceService(Socket client, PrintWriter out,
                                         String requestJson) {
        DataRequest<GameResourceRequest> request =
                new Gson().fromJson(requestJson,
                        new TypeToken<DataRequest<GameResourceRequest>>() {
                        }.getType());
        switch (request.getAction()) {
            case CommunicationProtocol.COLLECT:
                out.println(new Gson().toJson(resourceData.collect(
                        request.getData().getGameResource().getId(),
                        request.getData().getUserId(),
                        request.getData().getGeoPoint()
                )));
                break;
            case CommunicationProtocol.GET_ALL_BY_CLAN:
                out.println(new Gson().toJson(resourceData.getAllByClan(
                        request.getData().getClan()
                )));
                break;
            case CommunicationProtocol.GET_ALL:
                out.println(new Gson().toJson(resourceData.getAll(
                        request.getData().getGameResource().getGeoPoint()
                )));
                break;
        }
    }


}
