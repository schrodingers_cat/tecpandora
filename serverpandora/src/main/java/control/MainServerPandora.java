package control;

import java.io.IOException;
import java.util.Properties;

import bohr.engine.util.Util;
import bohr.engine.util.list.List;
import game.model.main.ServerGame;
import bohr.engine.admin.User;
import game.model.entities.others.GameRequest;
import game.model.entities.others.GameResource;
import game.model.entities.clan.Relic;
import model.dao.ActionDAOList;
import model.dao.GameRequestDAOList;
import model.dao.MessagesDAOList;
import model.dao.UserDAOList;
import model.dao.ClanDAOList;
import model.dao.GameResourceDAOList;
import model.socket.SocketAdmin;
import game.model.entities.clan.Clan;

/**
 * Se crean todas las listas que representa todos los objetos en el juegos
 * Clanes, jugadores, recusrsos del juuego, usuarios,reliquias, mensajes
 *
 * @author Isaac Trejos
 * @author Luis Murillo
 * @author Kevin Arce
 * @author Randall Alfaro
 */
public class MainServerPandora {
    //private static ArrayList<User> usersList = new ArrayList<>();
    private static List<User> usersList = new List<>();
    private static List<Clan> clansList = new List<>();
    private static List<GameResource> resourceList = new List<>();
    private static List<GameRequest> requestList = new List<>();
    private static List<Relic> relics = new List<>();

    /**
     * Clase constructura instancia y incia todos los daos necesarios para el juego.
     * Usuarios, clanes, recursos etc
     * Set de las properties
     *
     * @param args
     */
    public static void main(String[] args) {
        Properties properties = Util.getProperties("config.properties");
        //ArrayList<GameRequest> requestsDAO = new ArrayList<>();
        ServerGame serverGame = new ServerGame(clansList, usersList, resourceList);
        //properties);
        // Acceso a información sobre los datos
        UserDAOList playersDAO = new UserDAOList(usersList);
        ClanDAOList clansDAO = new ClanDAOList(clansList, usersList, relics);
        MessagesDAOList messagesDAO = new MessagesDAOList(clansList);
        GameResourceDAOList resourcesDAO = new GameResourceDAOList(clansList, usersList,
                resourceList);
        ActionDAOList actionDAO = new ActionDAOList(clansList, usersList, messagesDAO);
        GameRequestDAOList requestsDAO = new GameRequestDAOList(
                requestList, usersList, clansList, messagesDAO);
        //GameDAOList games = new GameDAOList(sessionsList, playersList);

        //SET PROPERTIES
        resourcesDAO.setProperties(properties);
        serverGame.setProperties(properties);
        actionDAO.setProperties(properties);

        // Acceso desde sockets
        SocketAdmin socketAdmin;

        try {
            // Instanciaci�n de los sockets
            socketAdmin = new SocketAdmin(
                    playersDAO, clansDAO, resourcesDAO,
                    requestsDAO, actionDAO,
                    messagesDAO, properties);

            // Ejecuci�n de los hilos de los sockets
            socketAdmin.start();

        } catch (IOException e) {
            System.out.println("Error while initiating socket: " + e);
        }
    }
}
