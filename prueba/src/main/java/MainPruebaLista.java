import bohr.engine.util.list.List;
import bohr.engine.util.list.Node;

public class MainPruebaLista {
    private static List list = new List();

    public static void main(String[] args) {
        System.out.println("------------");
        list.add(33);
        //System.out.println(list.head.getData());
        list.add(44);
        list.add(41);
        list.add(42);
        list.add(43);
        list.add(44);
        list.add(45);
        list.add(46);
        list.add(47);
        list.add(48);
        list.remove(5);
        buscar();
        getListRemove();
        recorrer();
        //getlist();
        System.out.println("-------------");
    }

    private static void getListRemove() {
        list.remove(48);
    }

    private static void getlist() {
        list.add(33);
        System.out.println("*+*+*+*+*");
        recorrer();
    }

    public static void buscar() {
        list.search(33);
        System.out.println(list.search(48));
        System.out.println("***********");
    }

    public static void recorrer() {
        Node tmp = list.head;
        while (tmp != null) {
            System.out.println(tmp.getData());
            tmp = tmp.getNextNode();
        }
    }
}
