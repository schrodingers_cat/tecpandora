import com.google.gson.Gson;

import bohr.engine.admin.User;
import bohr.engine.net.Network;
import bohr.engine.net.protocol.DataRequest;
import bohr.engine.net.protocol.DataResponse;
import bohr.engine.util.Util;
import game.model.entities.clan.Clan;
import game.model.entities.others.GameRequest;
import game.model.main.ClientGame;
import game.model.net.gameEvents.ClanEvents;
import game.model.net.gameEvents.RequestEvents;
import game.model.net.gameEvents.ResourcesEvents;
import bohr.engine.admin.GeoPoint;
import game.model.net.gameEvents.UserEvents;

/**
 * Created by curso on 09/09/2015.
 */
public class MainPrueba {
    private static ClientGame clientGame = new ClientGame();
    private static Network net;

    public static void main(String[] args) {
        clientGame.getEngineState().setPort(8080);
        net = clientGame.getNetwork();
        net.openConnection();
        testRequests();
        net.closeConnection();
    }

    private static void testRequests() {
        //USERS "A" Y "B"
        DataResponse<User> uA = UserEvents.signUp(net, "A", "123");
        DataResponse<User> uB = UserEvents.signUp(net, "B", "123");
        DataResponse<User> uC = UserEvents.signUp(net, "C", "123");

        //SE CREA EL CLAN "cA"
        DataResponse<Clan> cA = ClanEvents.createClan(
                clientGame.getNetwork(),
                0, "Ca", uA.getData().getId(),
                new GeoPoint(Util.randomPoint(9, 10),
                        -1 * Util.randomPoint(83, 85))
        );

        //USER "B" ENVIA REQUEST A CLAN "cA"
        DataResponse<GameRequest> rCauB =
                RequestEvents.sendRequest(
                        net, cA.getData().getId(), uB.getData().getId());

        //CLAN "cA" ACEPTA A MIEMBRO "B"
        RequestEvents.responseRequest(net, rCauB.getData().getIdRequest(), true);

        //USER "C" ENVIA REQUEST A CLAN "cA" Y QUEDA PENDIENTE
        DataResponse<GameRequest> rCauC =
                RequestEvents.sendRequest(
                        net, cA.getData().getId(), uC.getData().getId());

        //CLAN "cA" CON DUEÑO "A", UN MIEMBRO MÁS Y
        //UN REQUEST PENDIENTE QUE SOLO PUEDE VER EL CREADOR DEL CLAN
    }

/*
    private static void pruebaMessaging() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.sendMessage(1, 1, "hola"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.sendMessage(2, 1, "soy"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.sendMessage(3, 1, "el jugador X ha entrado al clan"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.sendMessage(4, 1, "peersona"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.sendMessage(6, 2, "jsdnfl"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllMessaging(1))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllMessaging(2))));
    }

    public static void pruebaResources() {

        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.collect(1, 1, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllClans())));
        int i = 18;
        int id = 1;
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.collect(
                        i + 1, id, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.collect(
                        i + 2, id, new GeoPoint()))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.collect(
                        i + 3, id, new GeoPoint()))));


        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllResources(new GeoPoint()))));

       System.out.println(clientGame.getNetwork().
                sendEvent(new Gson().toJson(
                        ResourcesEvents.getClan(
                                "Xyy"))));
    }

    public static void pruebaClan() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los guapos",
                        1, new GeoPoint(11.54, -84.1725)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los otros",
                        1, new GeoPoint(11.58654, -83.8665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllClans(new GeoPoint()))));
    }

    public static void pruebaUser() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall1", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall2", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.login("Randall2", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.login("Randall1", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.getAllUsers())));
    }

    public static void pruebaRequest() {
        int clan = 10;
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 1))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 3))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 4))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 5))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 6))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 7))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(clan, 8))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(1, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.getAllbyClanRequests(clan))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.getAllbyUserRequests(2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(1, false))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(2, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(3, false))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.getAllbyClanRequests(1))));
    }

    public static void pruebaAction() {

        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.buy(1, 1, 1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllByUserClanActions(1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 1, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 1, true)
        )));

        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.getAllByUserClanActions(1)
        )));
    }

    public static void createUsersClans() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall2", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall3", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall4", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall5", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los guapos",
                        1, new GeoPoint(11.54, -84.1725)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los feos",
                        1, new GeoPoint(10.54554, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los otros",
                        1, new GeoPoint(11.58654, -83.8665)))));
    }

    public static void pruebaDeVotosYCompras() {
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall1", "1233"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall2", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall3", "12334"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall4", "12335"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.signUp("Randall5", "456"))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.createClan(
                        1, "De los pichudos",
                        1, new GeoPoint(10.54, -85.1665)))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(1, 2))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(1, 3))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(1, 4))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.sendRequest(1, 5))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(1, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(2, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(3, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(ResourcesEvents.responseRequest(4, true))));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.buy(1, 1, 1)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 1, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 2, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 3, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 4, true)
        )));
        System.out.println("RECIEVED: " + net.sendEvent(new Gson().toJson(
                ResourcesEvents.voteGameAction(1, 5, true)
        )));

    }
*/

}
